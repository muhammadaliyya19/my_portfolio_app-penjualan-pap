<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cek_stok_gudang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Cek_stok_gudang_model');
        $this->load->model('Produk_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', true));
        $start = intval($this->input->get('start'));

        if ($q != '') {
            $config['base_url'] = base_url() . 'cek_stok_gudang/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'cek_stok_gudang/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'cek_stok_gudang/index.html';
            $config['first_url'] = base_url() . 'cek_stok_gudang/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = true;
        $config['total_rows'] = $this->Cek_stok_gudang_model->total_rows($q);
        $cek_stok_gudang = $this->Cek_stok_gudang_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'cek_stok_gudang_data' => $cek_stok_gudang,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'user' => $this->session->userdata('user'),
        );

        $data['judul'] = 'Data Cek Stok Gudang';

        $this->load->view('templates/header', $data);
        $this->load->view('cek_stok_gudang/cek_stok_gudang_list', $data);
        $this->load->view('templates/footer', $data);
    }

    public function read($id)
    {
        $row = $this->Cek_stok_gudang_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'id_produk' => $row->id_produk,
                'jumlah_cek' => $row->jumlah_cek,
                'jenis' => $row->jenis,
                'status' => $row->status,
                'created_at' => $row->created_at,
                'modified_at' => $row->modified_at,
                'user' => $this->session->userdata('user'),
            );

            $data['judul'] = 'Detail Cek Stok Gudang';

            $this->load->view('templates/header', $data);
            $this->load->view('cek_stok_gudang/cek_stok_gudang_read', $data);
            $this->load->view('templates/footer', $data);
        } else {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect(site_url('cek_stok_gudang'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('cek_stok_gudang/create_action'),
            'id' => set_value('id'),
            'id_produk' => set_value('id_produk'),
            'jumlah_cek' => set_value('jumlah_cek'),
            'jenis' => set_value('jenis'),
            'status' => set_value('status'),
            'created_at' => date('Y-m-d h:i:s'),
            'modified_at' => date('Y-m-d h:i:s'),
            'user' => $this->session->userdata('user'),
        );

        $data['judul'] = 'Permintaan Cek Stok Gudang';
        $data['list_produk'] = $this->Produk_model->get_all();

        $this->load->view('templates/header', $data);
        $this->load->view('cek_stok_gudang/cek_stok_gudang_form', $data);
        $this->load->view('templates/footer', $data);
    }

    public function create_action()
    {
        $idProduk = $this->input->post('id_produk', true);
        $produk = $this->Produk_model->get_by_id($idProduk);
        
        $data = array(
            'id_produk' => $idProduk,
            'stok_sekarang' => $produk->stok,
            'jenis' => $this->input->post('jenis', true),
            'status' => 0,
            'created_at' => date('Y-m-d h:i:s'),
            'modified_at' => date('Y-m-d h:i:s')
        );
        // var_dump($data); die;

        $this->Cek_stok_gudang_model->insert($data);
        $this->session->set_flashdata('success', 'Ditambah');
        redirect(site_url('cek_stok_gudang'));
    }

    public function update($id)
    {
        $row = $this->Cek_stok_gudang_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('cek_stok_gudang/update_action'),
                'id' => set_value('id', $row->id),
                'id_produk' => set_value('id_produk', $row->id_produk),
                'stok_sekarang' => set_value('jumlah_cek', $row->stok_sekarang),
                'jumlah_cek' => set_value('jumlah_cek', $row->jumlah_cek),
                'jenis' => set_value('jenis', $row->jenis),
                'status' => set_value('status', $row->status),
                'created_at' => set_value('created_at', $row->created_at),
                'modified_at' => set_value('modified_at', $row->modified_at),
                'user' => $this->session->userdata('user'),
            );

            $data['judul'] = 'Lapor Cek Stok Gudang';
            $data['list_produk'] = $this->Produk_model->get_all();

            $this->load->view('templates/header', $data);
            $this->load->view('cek_stok_gudang/cek_stok_gudang_form', $data);
            $this->load->view('templates/footer', $data);

        } else {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect(site_url('cek_stok_gudang'));
        }
    }

    public function update_action()
    {
        $status = $this->input->post('status', true);
        $idproduk = $this->input->post('id_produk', true);
        $row_produk = $this->Produk_model->get_by_id($idproduk);
        $jml_cek = (int)$this->input->post('jumlah_cek', true);

        $data = array(
            'jumlah_cek' => $jml_cek,
            'status' => $status,
            'modified_at' => date('Y-m-d h:i:s'),
        );

        $this->Cek_stok_gudang_model->update($this->input->post('id', true), $data);

        if($status == "2"){ // Jika status 2 (tidak lolos pengecekan, maka stok langsung diubah ke hasil pelaporan)
            $this->Produk_model->update_stok($id_produk, 'ubah', $jml_cek);
        }

        $this->session->set_flashdata('success', 'Diubah');
        redirect(site_url('cek_stok_gudang'));
    }

    public function approve_cek($id_cek_stok_gudang)
    {
        $row = $this->Cek_stok_gudang_model->get_by_id($id_cek_stok_gudang);
        $row_produk = $this->Produk_model->get_by_id($row->id_produk);

        // Jenis : CEK BARANG MASUK || PERMINTAAN CEK STOK

        $data_cek_gudang = array(
            'status' => 3,
            'modified_at' => date('Y-m-d h:i:s'),
        );

        $this->Cek_stok_gudang_model->update_approve_gudang($id_cek_stok_gudang, $data_cek_gudang);
        if($row->jenis == "PERMINTAAN CEK STOK"){
            $this->Produk_model->update_stok($row->id_produk, 'ubah', $row->jumlah_cek); // Mengubah stok berdasarkan hasil laporan
        } else {
            $this->Produk_model->update_stok($row->id_produk, 'tambah', $row->jumlah_cek); // Mengubah stok berdasarkan hasil laporan
        }

        $this->session->set_flashdata('success', 'Disetujui');
        redirect(site_url('cek_stok_gudang'));
    }

    public function delete($id)
    {
        $row = $this->Cek_stok_gudang_model->get_by_id($id);

        if ($row) {
            $this->Cek_stok_gudang_model->delete($id);
            $this->session->set_flashdata('success', 'Dihapus');
            redirect(site_url('cek_stok_gudang'));
        } else {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect(site_url('cek_stok_gudang'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('id_produk', 'id produk', 'trim|required|numeric');
        $this->form_validation->set_rules('jumlah_cek', 'jumlah cek', 'trim|required|numeric');
        $this->form_validation->set_rules('jenis', 'jenis', 'trim|required');
        $this->form_validation->set_rules('status', 'status', 'trim|required|numeric');
        $this->form_validation->set_rules('created_at', 'created at', 'trim|required');
        $this->form_validation->set_rules('modified_at', 'modified at', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "cek_stok_gudang.xls";
        $judul = "cek_stok_gudang";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Id Produk");
        xlsWriteLabel($tablehead, $kolomhead++, "Jumlah Cek");
        xlsWriteLabel($tablehead, $kolomhead++, "Jenis");
        xlsWriteLabel($tablehead, $kolomhead++, "Status");
        xlsWriteLabel($tablehead, $kolomhead++, "Created At");
        xlsWriteLabel($tablehead, $kolomhead++, "Modified At");

        foreach ($this->Cek_stok_gudang_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteNumber($tablebody, $kolombody++, $data->id_produk);
            xlsWriteNumber($tablebody, $kolombody++, $data->jumlah_cek);
            xlsWriteLabel($tablebody, $kolombody++, $data->jenis);
            xlsWriteNumber($tablebody, $kolombody++, $data->status);
            xlsWriteLabel($tablebody, $kolombody++, $data->created_at);
            xlsWriteLabel($tablebody, $kolombody++, $data->modified_at);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Cek_stok_gudang.php */
/* Location: ./application/controllers/Cek_stok_gudang.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2022-05-10 21:07:30 */
/* http://harviacode.com */
