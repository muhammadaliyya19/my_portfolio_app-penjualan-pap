<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Produk extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Produk_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'produk/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'produk/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'produk/index.html';
            $config['first_url'] = base_url() . 'produk/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Produk_model->total_rows($q);
        $produk = $this->Produk_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'produk_data' => $produk,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul' => 'Produk',
            'user' => $this->session->userdata('user')
        );

        // echo "disini";die;
        $this->load->view('templates/header', $data);
        $this->load->view('produk/produk_list', $data);
        $this->load->view('templates/footer', $data);
    }

    public function read($id) 
    {
        $row = $this->Produk_model->get_by_id($id);
        if ($row) {
            $foto_detail = $this->Produk_model->get_detail_by_produk($row->id);
            $data = array(
                'id' => $row->id,
                'nama' => $row->nama,
                'harga' => $row->harga,
                'foto' => $row->foto,
                'stok' => $row->stok,
                'keterangan' => $row->keterangan,
                'created_at' => $row->created_at,
                'modified_at' => $row->modified_at,
                'judul' => 'Detail Produk',
                'method' => 'read',
                'detail' => $foto_detail,
                'user' => $this->session->userdata('user')
            );

            $this->load->view('templates/header', $data);
            $this->load->view('produk/produk_read', $data);
            $this->load->view('templates/footer', $data);
        } else {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect(site_url('produk'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('produk/create_action'),
            'id' => set_value('id'),
            'nama' => set_value('nama'),
            'harga' => set_value('harga'),
            'foto' => set_value('foto'),
            'stok' => set_value('stok'),
            'keterangan' => set_value('keterangan'),
            'created_at' => set_value('created_at'),
            'modified_at' => set_value('modified_at'),
            'judul' => 'Tambah Produk',
            'method' => 'create',
            'user' => $this->session->userdata('user')
        );

        $this->load->view('templates/header', $data);
        $this->load->view('produk/produk_form', $data);
        $this->load->view('templates/footer', $data);
    }

    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            // Urusan upload master produk
            $nama_file = $this->do_upload();    
            $data = array(
                'nama' => $this->input->post('nama',TRUE),
                'harga' => $this->input->post('harga',TRUE),
                'foto' => $nama_file,
                'stok' => $this->input->post('stok',TRUE),
                'keterangan' => $this->input->post('keterangan',TRUE),
                'created_at' => $this->input->post('created_at',TRUE),
                'modified_at' => $this->input->post('modified_at',TRUE),
            );
            $this->Produk_model->insert($data);
            
            // Upload detail produk
            if (isset($_FILES['foto_detail'])) {
                # code...
                // Persiapan array data untuk dimasukkan ke DB
                $jumlah_detail_produk = count($this->input->post('counter'));
                $data_detail_produk = [];
                // Proses pengupload an gambar detail galeri
                $foto_detail_produk = $this->do_upload_detail();
                // Populating array detail galeri
                for($i = 0; $i < $jumlah_detail_produk; $i++){
                    $data_detail_produk[$i]['path'] = $foto_detail_produk[$i];                              
                }   
                // var_dump($data_detail_produk);         
                // Batas proses detail galeri
                // Memasukkan data galeri - foto utama

                // Memasukkan data detail galeri
                $this->Produk_model->tambah_detail_foto($data_detail_produk);
                // die;   
            }


            $this->session->set_flashdata('success', 'Ditambah');
            redirect(site_url('produk'));
        }
    }

    public function update($id) 
    {
        $row = $this->Produk_model->get_by_id($id);

        if ($row) {
            $foto_detail = $this->Produk_model->get_detail_by_produk($row->id);
            $data = array(
                'button' => 'Update',
                'action' => site_url('produk/update_action'),
                'id' => set_value('id', $row->id),
                'nama' => set_value('nama', $row->nama),
                'harga' => set_value('harga', $row->harga),
                'foto' => set_value('foto', $row->foto),
                'stok' => set_value('stok', $row->stok),
                'keterangan' => set_value('keterangan', $row->keterangan),
                'created_at' => set_value('created_at', $row->created_at),
                'modified_at' => set_value('modified_at', $row->modified_at),
                'detail' => $foto_detail,
                'judul' => 'Ubah Produk',
                'method' => 'update',
                'user' => $this->session->userdata('user')
            );


            $this->load->view('templates/header', $data);
            $this->load->view('produk/produk_form', $data);
            $this->load->view('templates/footer', $data);

        } else {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect(site_url('produk'));
        }
    }

    public function update_action() 
    {
        // ACTION UPDATE
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $nama_file = $this->do_upload();    
            $data = array(
                'nama' => $this->input->post('nama',TRUE),
                'harga' => $this->input->post('harga',TRUE),
                'stok' => $this->input->post('stok',TRUE),
                'keterangan' => $this->input->post('keterangan',TRUE),
                'modified_at' => $this->input->post('modified_at',TRUE),
            );
            if ($nama_file != NULL) {
                $data['foto'] = $nama_file;                
            }
            $this->Produk_model->update($this->input->post('id', TRUE), $data);
            
            if (isset($_FILES['foto_detail'])) {
                $jumlah_detail_produk = count($this->input->post('counter'));
                $data_detail_produk = [];
                $foto_detail_produk = $this->do_upload_detail();
                for($i = 0; $i < $jumlah_detail_produk; $i++){
                    $data_detail_produk[$i]['path'] = $foto_detail_produk[$i];                              
                }   
                $this->Produk_model->tambah_detail_foto($data_detail_produk);
            }

            $this->session->set_flashdata('success', 'Diubah');
            redirect(site_url('produk'));
        }
    }

    public function delete($id) 
    {
        $row = $this->Produk_model->get_by_id($id);
        $prod_detail = $this->Produk_model->get_detail_by_produk($id);

        if ($row) {
            // Delete Data
            $this->Produk_model->delete($id);
            // Delete Detali Data
            $this->Produk_model->delete_detail_batch($id);

            // Unlink / hapus file
            // Unlink / hapus file main produk
            unlink('./assets/img/produk/'.$row->foto);            
            // Unlink / hapus file detil produk
            foreach ($prod_detail as $dtl) {
                unlink('./assets/img/produk/'.$dtl->path);            
            }

            $this->session->set_flashdata('success', 'Dihapus');
            redirect(site_url('produk'));
        } else {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect(site_url('produk'));
        }
    }

    public function delete_detail($iddetail) 
    {
        $row = $this->Produk_model->get_detail_by_id($iddetail);
        unlink('./assets/img/produk/'.$row->path);            
        $this->Produk_model->delete_detail($row->id);
    }

    public function _rules() 
    {
        $this->form_validation->set_rules('nama', 'nama', 'trim|required');
        $this->form_validation->set_rules('harga', 'harga', 'trim|required|numeric');
        $this->form_validation->set_rules('stok', 'stok', 'trim|required|numeric');
        $this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');
        $this->form_validation->set_rules('created_at', 'created at', 'trim|required');
        $this->form_validation->set_rules('modified_at', 'modified at', 'trim|required');
        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "produk.xls";
        $judul = "produk";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama");
        xlsWriteLabel($tablehead, $kolomhead++, "Harga");
        xlsWriteLabel($tablehead, $kolomhead++, "Foto");
        xlsWriteLabel($tablehead, $kolomhead++, "Stok");
        xlsWriteLabel($tablehead, $kolomhead++, "Keterangan");
        xlsWriteLabel($tablehead, $kolomhead++, "Created At");
        xlsWriteLabel($tablehead, $kolomhead++, "Modified At");

        foreach ($this->Produk_model->get_all() as $data) {
            $kolombody = 0;

            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama);
            xlsWriteNumber($tablebody, $kolombody++, $data->harga);
            xlsWriteLabel($tablebody, $kolombody++, $data->foto);
            xlsWriteNumber($tablebody, $kolombody++, $data->stok);
            xlsWriteLabel($tablebody, $kolombody++, $data->keterangan);
            xlsWriteLabel($tablebody, $kolombody++, $data->created_at);
            xlsWriteLabel($tablebody, $kolombody++, $data->modified_at);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

// The Do Upload Settings
    public function do_upload()
    {       
        $this->load->library('upload');
        if ($_FILES['foto']['size'] > 0) {
            $files = $_FILES['foto'];
            $config = $this->set_upload_options();
            $config['file_name'] = time().'-'.date("Y-m-d-his").'.jpg';         
            $this->upload->initialize($config);
            $res = $this->upload->do_upload('foto');
            return $config['file_name'];
        }
    }

// The Do Upload Settings
    public function do_upload_detail()
    {       
        $this->load->library('upload');

        $files = $_FILES;
        $cpt = count($_FILES['foto_detail']['name']);
        $config = [];
        $new_nama_img = [];
        // echo "<br>COUNT Detail : ".$cpt."<br>";
        // echo "<br>Data Raw Detail<br>";
        // var_dump($_FILES['foto_detail']);
        for($i=0; $i<$cpt; $i++)
        {           
            if ($_FILES['foto_detail']['name'][$i] != "") {
                $config = $this->set_upload_options();
                $config['file_name'] = time().'-'.date("Y-m-dhis").$i.'.jpg';
                $_FILES['foto_detail']['name']= $files['foto_detail']['name'][$i];
                $_FILES['foto_detail']['type']= $files['foto_detail']['type'][$i];
                $_FILES['foto_detail']['tmp_name']= $files['foto_detail']['tmp_name'][$i];
                $_FILES['foto_detail']['error']= $files['foto_detail']['error'][$i];
                $_FILES['foto_detail']['size']= $files['foto_detail']['size'][$i];    
                array_push($new_nama_img, $config['file_name']);
                $this->upload->initialize($config);
                $res = $this->upload->do_upload('foto_detail');
            }
        // echo $res . "<br><br>Foto " . $i . " sukses diupload<br><br>";
        // var_dump($_FILES['foto_detail']['size']);
        // echo $res . "<br><br>Foto " . $i . " sukses diupload<br><br>";
        // var_dump($_FILES['foto_detail']);
        }
        // var_dump($new_nama_img);
        return $new_nama_img;
    }

    private function set_upload_options()
    {   
    //upload an image options
        $config = array();
        $config['upload_path'] = './assets/img/produk/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']      = '120000';
        $config['overwrite']     = FALSE;
        return $config;
    }
    // Batas upload

}

/* End of file Produk.php */
/* Location: ./application/controllers/Produk.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2022-05-10 15:13:17 */
                        /* http://harviacode.com */