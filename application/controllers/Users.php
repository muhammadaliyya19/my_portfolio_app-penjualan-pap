<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'users/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'users/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'users/index.html';
            $config['first_url'] = base_url() . 'users/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Users_model->total_rows($q);
        $users = $this->Users_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'users_data' => $users,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );

        $data['judul'] = 'Data User';
        $data['user'] = $this->session->userdata('user');

        $this->load->view('templates/header', $data);
        $this->load->view('users/user_list', $data);
        $this->load->view('templates/footer', $data);
    }

    public function read($id) 
    {
        $row = $this->Users_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id_user,
                'nama' => $row->nama_user,
                'email' => $row->email,
                'alamat' => $row->alamat,
                'telepon' => $row->telepon,
                'jk' => $row->jk,
                'method' => 'read',
                'nama_role' => $row->nama_role,
                'created_at' => $row->created_at,
                'modified_at' => $row->modified_at,
            );

            $data['judul'] = 'Detail User';
            $data['user'] = $this->session->userdata('user');

            $this->load->view('templates/header', $data);
            $this->load->view('users/user_read', $data);
            $this->load->view('templates/footer', $data);
        } else {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect(site_url('users'));
        }
    }

    public function create() 
    {
        $roles = $this->Users_model->get_all_role();
        $data = array(
            'button' => 'Create',
            'action' => site_url('users/create_action'),
            'id' => set_value('id'),
            'nama' => set_value('nama'),
            'email' => set_value('email'),
            'telepon' => set_value('telepon'),
            'alamat' => set_value('alamat'),
            'jk' => set_value('jk'),
            'method' => 'create',
            'role' => set_value('role'),
            'roles' => $roles,
            'created_at' => set_value('created_at'),
            'modified_at' => set_value('modified_at'),
        );

        $data['judul'] = 'Tambah User';
        $data['user'] = $this->session->userdata('user');

        $this->load->view('templates/header', $data);
        $this->load->view('users/user_form', $data);
        $this->load->view('templates/footer', $data);
    }

    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'nama_user' => $this->input->post('nama',TRUE),
                'email' => $this->input->post('email',TRUE),
                'telepon' => $this->input->post('telepon',TRUE),
                'alamat' => $this->input->post('alamat',TRUE),
                'jk' => $this->input->post('jk',TRUE),
                'id_role' => $this->input->post('role',TRUE),
                'password' => password_hash($this->input->post('password',TRUE), PASSWORD_DEFAULT),
                'gambar' => 'man-1.png',
                'created_at' => $this->input->post('created_at',TRUE),
                'modified_at' => $this->input->post('modified_at',TRUE),
            );          

            $this->Users_model->insert($data);
            $this->session->set_flashdata('success', 'Ditambah');
            redirect(site_url('users'));
        }
    }

    public function update($id) 
    {
        $roles = $this->Users_model->get_all_role();
        $row = $this->Users_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('users/update_action'),
                'id' => set_value('id', $row->id_user),
                'nama' => set_value('nama', $row->nama_user),
                'email' => set_value('email', $row->email),
                'telepon' => set_value('telepon', $row->telepon),
                'alamat' => set_value('alamat', $row->alamat),
                'jk' => set_value('jk', $row->jk),
                'method' => 'update',
                'role' => set_value('role', $row->id_role),
                'roles' => $roles,
                'created_at' => set_value('created_at', $row->created_at),
                'modified_at' => set_value('modified_at', $row->modified_at),
            );

            $data['judul'] = 'Ubah User';
            $data['user'] = $this->session->userdata('user');

            $this->load->view('templates/header', $data);
            $this->load->view('users/user_form', $data);
            $this->load->view('templates/footer', $data);

        } else {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect(site_url('users'));
        }
    }

    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $password = $this->input->post('password',TRUE);
            $data = array(
                'nama_user' => $this->input->post('nama',TRUE),
                'email' => $this->input->post('email',TRUE),
                'telepon' => $this->input->post('telepon',TRUE),
                'alamat' => $this->input->post('alamat',TRUE),
                'jk' => $this->input->post('jk',TRUE),
                'id_role' => $this->input->post('role',TRUE),
                'created_at' => $this->input->post('created_at',TRUE),
                'modified_at' => $this->input->post('modified_at',TRUE),
            );

            if (strlen($password) > 0) {
                $data['password'] = password_hash($password, PASSWORD_DEFAULT);
            }

            $this->Users_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('success', 'Diubah');
            redirect(site_url('users'));
        }
    }

    public function delete($id) 
    {
        $row = $this->Users_model->get_by_id($id);

        if ($row) {
            $this->Users_model->delete($id);
            $this->session->set_flashdata('success', 'Dihapus');
            redirect(site_url('users'));
        } else {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect(site_url('users'));
        }
    }

    public function _rules() 
    {
        $this->form_validation->set_rules('nama', 'nama', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        $this->form_validation->set_rules('telepon', 'telepon', 'trim|required');
        $this->form_validation->set_rules('jk', 'jk', 'trim|required');
        $this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
        $this->form_validation->set_rules('role', 'role', 'trim|required|numeric');
        $this->form_validation->set_rules('created_at', 'created at', 'trim|required');
        $this->form_validation->set_rules('modified_at', 'modified at', 'trim|required');
        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "user_v0.xls";
        $judul = "user_v0";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama");
        xlsWriteLabel($tablehead, $kolomhead++, "Email");
        xlsWriteLabel($tablehead, $kolomhead++, "Username");
        xlsWriteLabel($tablehead, $kolomhead++, "Password");
        xlsWriteLabel($tablehead, $kolomhead++, "Alamat");
        xlsWriteLabel($tablehead, $kolomhead++, "Role");
        xlsWriteLabel($tablehead, $kolomhead++, "Created At");
        xlsWriteLabel($tablehead, $kolomhead++, "Modified At");

        foreach ($this->Users_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama);
            xlsWriteLabel($tablebody, $kolombody++, $data->email);
            xlsWriteLabel($tablebody, $kolombody++, $data->username);
            xlsWriteLabel($tablebody, $kolombody++, $data->password);
            xlsWriteLabel($tablebody, $kolombody++, $data->alamat);
            xlsWriteNumber($tablebody, $kolombody++, $data->role);
            xlsWriteLabel($tablebody, $kolombody++, $data->created_at);
            xlsWriteLabel($tablebody, $kolombody++, $data->modified_at);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Users.php */
/* Location: ./application/controllers/Users.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2022-05-10 21:08:07 */
                        /* http://harviacode.com */