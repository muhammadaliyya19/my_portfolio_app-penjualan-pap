<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pesanan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Pesanan_model');
        $this->load->model('Users_model');
        $this->load->model('Produk_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', true));
        $start = intval($this->input->get('start'));

        if ($q != '') {
            $config['base_url'] = base_url() . 'pesanan/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pesanan/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'pesanan/index.html';
            $config['first_url'] = base_url() . 'pesanan/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = true;
        $config['total_rows'] = $this->Pesanan_model->total_rows($q);
        $pesanan = $this->Pesanan_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'pesanan_data' => $pesanan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );

        $data['user'] = $this->session->userdata('user');
        $data['judul'] = $data['user']['level'] == "Customer" ? 'Pesanan Saya' : 'Pesanan';

        $this->load->view('templates/header', $data);
        $this->load->view('pesanan/pesanan_list', $data);
        $this->load->view('templates/footer', $data);
    }

    public function read($id)
    {
        $row = $this->Pesanan_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'kode_pesanan' => $row->kode_pesanan,
                'pembeli' => $row->nama_user,
                'produk' => $row->namaproduk,
                'jumlah' => $row->jumlah,
                'status' => $row->status,
                'created_at' => $row->created_at,
                'modified_at' => $row->modified_at,
            );

            $data['judul'] = 'Detail Pesanan';
            $data['user'] = $this->session->userdata('user');

            $this->load->view('templates/header', $data);
            $this->load->view('pesanan/pesanan_read', $data);
            $this->load->view('templates/footer', $data);
        } else {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect(site_url('pesanan'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('pesanan/create_action'),
            'id' => set_value('id'),
            'kode_pesanan' => $this->generateTrxID(),
            'pembeli' => set_value('pembeli'),
            'id_produk' => set_value('id_produk'),
            'jumlah' => set_value('jumlah'),
            'status' => set_value('status'),
            'created_at' => set_value('created_at'),
            'modified_at' => set_value('modified_at'),
        );

        $data['judul'] = 'Tambah Pesanan';
        $data['user'] = $this->session->userdata('user');
        $data['list_pembeli'] = $this->Users_model->get_cust();
        $data['list_produk_avail'] = $this->Produk_model->get_avail_prod();
        // print_r($data['list_produk_avail']); die;
        $this->load->view('templates/header', $data);
        $this->load->view('pesanan/pesanan_form', $data);
        $this->load->view('templates/footer', $data);
    }

    public function generateTrxID($length = 6)
    {
        $result = '';
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $charactersLength = strlen($characters);
        for ($i = 0; $i < $length; $i++) {
            $result .= $characters[rand(0, $charactersLength - 1)];
        }
        return $result . date('YmdHis');
    }

    public function create_action()
    {
        $this->_rules();
        $reqFrom = isset($_POST['q']);
        if ($this->form_validation->run() == false) {
            $this->create();
        } else {
            $data = array(
                'kode_pesanan' => $this->input->post('kode_pesanan', true),
                'pembeli' => $this->input->post('pembeli', true),
                'id_produk' => $this->input->post('id_produk', true),
                'jumlah' => $this->input->post('jumlah', true),
                'status' => 0,
                'created_at' => date('Y-m-d h:i:s'),
                'modified_at' => date('Y-m-d h:i:s'),
            );

            $this->Pesanan_model->insert($data);
            if ($reqFrom) {
                $this->session->set_flashdata('success', 'Sukses. Silakan Checkout Untuk Melanjutkan Proses Pesanan!');
            } else {
                $this->session->set_flashdata('success', 'Ditambah');
            }
            redirect(site_url('pesanan'));
        }
    }

    public function update($id)
    {
        $row = $this->Pesanan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('pesanan/update_action'),
                'id' => set_value('id', $row->id),
                'kode_pesanan' => set_value('kode_pesanan', $row->kode_pesanan),
                'pembeli' => set_value('pembeli', $row->pembeli),
                'id_produk' => set_value('id_produk', $row->id_produk),
                'jumlah' => set_value('jumlah', $row->jumlah),
                'status' => set_value('status', $row->status),
                'invoice' => set_value('invoice', $row->invoice),
                'created_at' => set_value('created_at', $row->created_at),
                'modified_at' => set_value('modified_at', $row->modified_at),
            );

            $data['judul'] = 'Update Pesanan';
            $data['user'] = $this->session->userdata('user');
            $data['list_pembeli'] = $this->Users_model->get_cust();
            $data['list_produk_avail'] = $this->Produk_model->get_all();

            $this->load->view('templates/header', $data);
            $this->load->view('pesanan/pesanan_form', $data);
            $this->load->view('templates/footer', $data);

        } else {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect(site_url('pesanan'));
        }
    }

    public function update_action()
    {
        $id = $this->input->post('id', true);
        $status = (int)$this->input->post('status', true);
        $row = $this->Pesanan_model->get_by_id($id);
        $data = array(
            'status' => $status,
            'modified_at' => date('Y-m-d h:i:s'),
        );

        $upFile = $this->do_upload($row);    

        if (isset($_POST['q']) && $_POST['q'] == "checkout_user") {
            // var_dump("disini 190"); 
            // var_dump($_FILES); 
            // die;

            // Upload bukti bayar disini
            if ($upFile['result']) {
                $data['bukti_transfer'] = $upFile['file_name'];
                $this->session->set_flashdata('success', 'Dibuat dan segera diproses.');
                // $this->session->set_flashdata('success', 'Diupdate. ');
            } else {
                $data['bukti_transfer'] = NULL;
                $this->session->set_flashdata('failed', 'Diupdate');
            }
        }else{
            if ($upFile['result']) {
                $data['invoice'] = $upFile['file_name'];
                $this->session->set_flashdata('success', 'Diupdate. ');
            } else {
                $data['invoice'] = NULL;
                $this->session->set_flashdata('failed', 'Diupdate');
            }
        }
        // var_dump("disini"); die;
        $this->Pesanan_model->update($this->input->post('id', true), $data);
        /*
            Update jumlah stok, jika status pesanan == 2 (Sedang dikirim, belum diterima maka ada pengurangan stok)
            Pengurangan stok di 'pesanan' hanya terjadi saat admin sudah mengupdate pesanan dan status sudah 2 ^^^^ vvvv
        */
        if($status == 2 && (int)$row->status < 2){
            $this->Produk_model->update_stok($row->id_produk, 'kurang', $row->jumlah);
        }
        redirect(site_url('pesanan'));
    }

    public function delete($id)
    {
        $row = $this->Pesanan_model->get_by_id($id);

        if ($row) {
            $this->Pesanan_model->delete($id);
            $this->session->set_flashdata('success', 'Dihapus');
            redirect(site_url('pesanan'));
        } else {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect(site_url('pesanan'));
        }
    }

    public function checkout($id)
    {
        $row = $this->Pesanan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Checkout',
                'action' => site_url('pesanan/update_action'),
                'id' => set_value('id', $row->id),
                'kode_pesanan' => set_value('kode_pesanan', $row->kode_pesanan),
                'pembeli' => set_value('pembeli', $row->pembeli),
                'id_produk' => set_value('id_produk', $row->id_produk),
                'namaproduk' => set_value('namaproduk', $row->namaproduk),
                'jumlah' => set_value('jumlah', $row->jumlah),
                'harga' => $row->harga,
                'total_harga' => $row->jumlah * $row->harga,
                'status' => set_value('status', $row->status),
                'modified_at' => set_value('modified_at', $row->modified_at),
            );

            $data['judul'] = 'Checkout Pesanan';
            $data['user'] = $this->session->userdata('user');
            $data['list_pembeli'] = $this->Users_model->get_cust();
            $data['list_produk_avail'] = $this->Produk_model->get_all();

            $this->load->view('templates/header', $data);
            $this->load->view('pesanan/checkout_form', $data);
            $this->load->view('templates/footer', $data);

        } else {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect(site_url('pesanan'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('kode_pesanan', 'kode pesanan', 'trim|required');
        $this->form_validation->set_rules('pembeli', 'pembeli', 'trim|required|numeric');
        $this->form_validation->set_rules('id_produk', 'id produk', 'trim|required|numeric');
        $this->form_validation->set_rules('jumlah', 'jumlah', 'trim|required|numeric');
        // $this->form_validation->set_rules('status', 'status', 'trim|required|numeric');
        // $this->form_validation->set_rules('created_at', 'created at', 'trim|required');
        // $this->form_validation->set_rules('modified_at', 'modified at', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    // The Do Upload Settings
    public function do_upload($row)
    {       
        $this->load->library('upload');
        $config = $this->set_upload_options();
        if ( isset($_FILES['invoice']['tmp_name']) ) {
            $kodePesanan = $row->kode_pesanan;
            $invName = $row->invoice == NULL ? $kodePesanan.'_'.date("Y_m_d_his").'.pdf' : $row->invoice;

            $config['file_name'] = 'Inv_'.$invName;

            // echo "line 300 <br><br>";
            // var_dump($config); die;
            $this->upload->initialize($config);            
            
            $res = $this->upload->do_upload('invoice');
            
            return array(
                'file_name' => $config['file_name'], 
                'result' => $res 
            );
        }else if ( file_exists($_FILES['bukti_transfer']['tmp_name']) ) {
            $kodePesanan = $row->kode_pesanan;
            $path = $_FILES['bukti_transfer']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $bukti_trf = $kodePesanan.'_'.date("Y_m_d_his").'.'.$ext;

            $config['file_name'] = 'Trf_'.$bukti_trf;

            // echo "line 318 <br><br>";
            // var_dump($config); die;

            $this->upload->initialize($config);            
            
            $res = $this->upload->do_upload('bukti_transfer');
            
            return array(
                'file_name' => $config['file_name'], 
                'result' => $res 
            );
        }else{
            return array(
                'result' => false 
            );
        }
    }

    private function set_upload_options()
    {   
        
        //config upload an file
        $config = array();
        $config['upload_path'] = './assets/pdf/invoice/';
        $config['allowed_types'] = 'pdf';
        $config['max_size']      = '120000';
        $config['overwrite']     = TRUE;

        if( isset($_POST['q']) && $_POST['q'] == "checkout_user" ){
            $config['upload_path'] = './assets/img/bukti_trf/';
            $config['allowed_types'] = 'jpg|jpeg|png|pdf';
            $config['max_size']      = '20000';
        }
        return $config;
    }
    // Batas upload

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "pesanan.xls";
        $judul = "pesanan";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Kode Pesanan");
        xlsWriteLabel($tablehead, $kolomhead++, "Pembeli");
        xlsWriteLabel($tablehead, $kolomhead++, "Id Produk");
        xlsWriteLabel($tablehead, $kolomhead++, "Jumlah");
        xlsWriteLabel($tablehead, $kolomhead++, "Status");
        xlsWriteLabel($tablehead, $kolomhead++, "Created At");
        xlsWriteLabel($tablehead, $kolomhead++, "Modified At");

        foreach ($this->Pesanan_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->kode_pesanan);
            xlsWriteNumber($tablebody, $kolombody++, $data->pembeli);
            xlsWriteNumber($tablebody, $kolombody++, $data->id_produk);
            xlsWriteNumber($tablebody, $kolombody++, $data->jumlah);
            xlsWriteNumber($tablebody, $kolombody++, $data->status);
            xlsWriteLabel($tablebody, $kolombody++, $data->created_at);
            xlsWriteLabel($tablebody, $kolombody++, $data->modified_at);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Pesanan.php */
/* Location: ./application/controllers/Pesanan.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2022-05-10 21:07:39 */
/* http://harviacode.com */
