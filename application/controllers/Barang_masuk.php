<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Barang_masuk extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Barang_masuk_model');
        $this->load->model('Cek_stok_gudang_model');
        $this->load->model('Produk_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', true));
        $start = intval($this->input->get('start'));

        if ($q != '') {
            $config['base_url'] = base_url() . 'barang_masuk/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'barang_masuk/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'barang_masuk/index.html';
            $config['first_url'] = base_url() . 'barang_masuk/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = true;
        $config['total_rows'] = $this->Barang_masuk_model->total_rows($q);
        $barang_masuk = $this->Barang_masuk_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'barang_masuk_data' => $barang_masuk,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'user' => $this->session->userdata('user'),
        );

        $data['judul'] = 'Data Barang Masuk';

        $this->load->view('templates/header', $data);
        $this->load->view('barang_masuk/barang_masuk_list', $data);
        $this->load->view('templates/footer', $data);
    }

    public function read($id)
    {
        $row = $this->Barang_masuk_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'id_produk' => $row->id_produk,
                'namaproduk' => $row->namaproduk,
                'jumlah' => $row->jumlah,
                'status' => $row->status,
                'created_at' => $row->created_at,
                'modified_at' => $row->modified_at,
                'user' => $this->session->userdata('user'),
            );

            $data['judul'] = 'Detail Barang Masuk';

            $this->load->view('templates/header', $data);
            $this->load->view('barang_masuk/barang_masuk_read', $data);
            $this->load->view('templates/footer', $data);
        } else {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect(site_url('barang_masuk'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('barang_masuk/create_action'),
            'id' => set_value('id'),
            'id_produk' => set_value('id_produk'),
            'jumlah' => set_value('jumlah'),
            'status' => set_value('status'),
            'created_at' => set_value('created_at'),
            'modified_at' => set_value('modified_at'),
            'user' => $this->session->userdata('user'),
        );

        $data['judul'] = 'Tambah Barang Masuk';
        $data['list_produk'] = $this->Produk_model->get_all();

        $this->load->view('templates/header', $data);
        $this->load->view('barang_masuk/barang_masuk_form', $data);
        $this->load->view('templates/footer', $data);
    }

    public function create_action()
    {
        // var_dump($_POST); die;
        $this->_rules();

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {
            $data = array(
                'id_produk' => $this->input->post('id_produk', true),
                'jumlah' => $this->input->post('jumlah', true),
                'status' => $this->input->post('status', true),
                'created_at' => date('Y-m-d h:i:s'),
                'modified_at' => date('Y-m-d h:i:s'),
            );

            $this->Barang_masuk_model->insert($data);
            $this->session->set_flashdata('success', 'Ditambah');
            redirect(site_url('barang_masuk'));
        }
    }

    public function update($id)
    {
        $row = $this->Barang_masuk_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('barang_masuk/update_action'),
                'id' => set_value('id', $row->id),
                'id_produk' => set_value('id_produk', $row->id_produk),
                'jumlah' => set_value('jumlah', $row->jumlah),
                'status' => set_value('status', $row->status),
                'created_at' => set_value('created_at', $row->created_at),
                'modified_at' => set_value('modified_at', $row->modified_at),
                'user' => $this->session->userdata('user'),
            );

            $data['judul'] = 'Ubah Barang Masuk';

            $this->load->view('templates/header', $data);
            $this->load->view('barang_masuk/barang_masuk_form', $data);
            $this->load->view('templates/footer', $data);

        } else {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect(site_url('barang_masuk'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            $this->update($this->input->post('id', true));
        } else {
            $data = array(
                'status' => $this->input->post('status', true),
                'modified_at' => date('Y-m-d h:i:s'),
            );

            $this->Barang_masuk_model->update($this->input->post('id', true), $data);
            $this->session->set_flashdata('success', 'Diubah');
            redirect(site_url('barang_masuk'));
        }
    }

    public function lapor_cek($id)
    {
        $row = $this->Barang_masuk_model->get_by_id($id);
        $row_produk = $this->Produk_model->get_by_id($row->id_produk);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('barang_masuk/lapor_cek_action'),
                'id' => set_value('id', $row->id),
                'id_produk' => set_value('id_produk', $row->id_produk),
                'namaproduk' => set_value('id_produk', $row_produk->nama),
                'jumlah' => set_value('jumlah', $row->jumlah),
                'status' => set_value('status', $row->status),
                'created_at' => set_value('created_at', $row->created_at),
                'modified_at' => set_value('modified_at', $row->modified_at),
                'user' => $this->session->userdata('user'),
            );

            $data['judul'] = 'Lapor Pengecekan Barang Masuk';

            $this->load->view('templates/header', $data);
            $this->load->view('barang_masuk/cek_barang_masuk_form', $data);
            $this->load->view('templates/footer', $data);

        } else {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect(site_url('barang_masuk'));
        }
    }

    public function lapor_cek_action()
    {
        // var_dump($_POST); die;
        $idBarangMasuk = $this->input->post('id', true);
        $rowBarangMasuk = $this->Barang_masuk_model->get_by_id($idBarangMasuk);
        $status = $this->input->post('status', true);
        $jml_cek = (int)$this->input->post('jumlah_cek', true);
        
        $data_cek_masuk = array(
            'status' => $status,
            'modified_at' => date('Y-m-d h:i:s'),
        );

        $id_produk = $this->input->post('id_produk', true);

        $data_cek_gudang = array(
            'id_produk' => $id_produk,
            'id_barang_masuk' => $this->input->post('id', true),
            'jumlah_cek' => $jml_cek,
            'jenis' => "CEK BARANG MASUK",
            'status' => $status,
            'created_at' => date('Y-m-d h:i:s'),
            'modified_at' => date('Y-m-d h:i:s'),
        );

        $this->Barang_masuk_model->update($this->input->post('id', true), $data_cek_masuk); //Update status laporan barang masuk
        
        // Memasukkan data barang masuk ke pengecekan gudang
        $this->Cek_stok_gudang_model->insert($data_cek_gudang);

        // cek, apakah status Lolos, atau TIDAK LOLOS. Jika lolos, maka stok produk ditambah sesuai yang dilaporkan produksi, 
        // jika tidak sesuai (2) maka stok ditambah berdasarkan hasil pengecekan gudang dan admin tidak perlu approve
        if ($status == "2"){
            $this->Produk_model->update_stok($id_produk, 'tambah', $jml_cek);
        }

        $this->session->set_flashdata('success', 'Dilaporkan');
        redirect(site_url('barang_masuk'));
    }

    public function approve_cek($id_barang_masuk)
    {
        $row = $this->Barang_masuk_model->get_by_id($id_barang_masuk);
        $row_produk = $this->Produk_model->get_by_id($row->id_produk);

        $data_cek_masuk = array(
            'status' => 3,
            'modified_at' => date('Y-m-d h:i:s'),
        );

        $data_cek_gudang = array(
            'status' => 3,
            'modified_at' => date('Y-m-d h:i:s'),
        );

        $this->Cek_stok_gudang_model->update_approve($id_barang_masuk, $data_cek_gudang);
        $this->Barang_masuk_model->update($id_barang_masuk, $data_cek_masuk);
        
        // Menambah stok berdasarkan hasil approve pengecekan
        $this->Produk_model->update_stok($row->id_produk, 'tambah', $row->jumlah);

        $this->session->set_flashdata('success', 'Disetujui');
        redirect(site_url('barang_masuk'));
    }

    public function delete($id)
    {
        $row = $this->Barang_masuk_model->get_by_id($id);

        if ($row) {
            $this->Barang_masuk_model->delete($id);
            $this->session->set_flashdata('success', 'Dihapus');
            redirect(site_url('barang_masuk'));
        } else {
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect(site_url('barang_masuk'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('id_produk', 'id produk', 'trim|required|numeric');
        $this->form_validation->set_rules('jumlah', 'jumlah', 'trim|required|numeric');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "barang_masuk.xls";
        $judul = "barang_masuk";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Id Produk");
        xlsWriteLabel($tablehead, $kolomhead++, "Jumlah");
        xlsWriteLabel($tablehead, $kolomhead++, "Status");
        xlsWriteLabel($tablehead, $kolomhead++, "Created At");
        xlsWriteLabel($tablehead, $kolomhead++, "Modified At");

        foreach ($this->Barang_masuk_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteNumber($tablebody, $kolombody++, $data->id_produk);
            xlsWriteNumber($tablebody, $kolombody++, $data->jumlah);
            xlsWriteNumber($tablebody, $kolombody++, $data->status);
            xlsWriteLabel($tablebody, $kolombody++, $data->created_at);
            xlsWriteLabel($tablebody, $kolombody++, $data->modified_at);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Barang_masuk.php */
/* Location: ./application/controllers/Barang_masuk.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2022-05-10 21:06:22 */
/* http://harviacode.com */
