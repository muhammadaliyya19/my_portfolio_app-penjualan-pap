<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="pull-left">
                    <div class="box-title">
                        <h4><?php echo $judul ?></h4>
                    </div>
                </div>
                <?php if($user['level'] != "Customer"):?>
                <div class="pull-right">
                    <div class="box-title">
                        <a href="<?php echo base_url('pesanan/create') ?>" class="btn btn-primary"><i
                                class="fa fa-plus"></i> Tambah Data</a>
                    </div>
                </div>
                <?php endif;?>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" width="100%" id="dt_pesanan">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Pesanan</th>
                                <?php if($user['level'] != "Customer"):?>
                                <th>Pembeli</th>
                                <?php endif;?>
                                <th>Alamat Pengiriman</th>
                                <th>Produk</th>
                                <th>Jumlah</th>
                                <th>Status</th>
                                <th>Bukti Transfer</th>
                                <th>Invoice</th>
                                <th>Tanggal Pemesanan</th>
                                <?php if($user['level'] != "Customer"):?>
                                <th>Tanggal Update</th>
                                <?php endif;?>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($pesanan_data as $pesanan)
                                {
                                    ?>
                            <tr>
                                <td><?php echo ++$start ?></td>
                                <td><?php echo $pesanan->kode_pesanan ?></td>
                                <?php if($user['level'] != "Customer"):?>
                                <td><a href="<?php echo site_url('users/read/' . $pesanan->pembeli ) ?>"><?php echo $pesanan->nama_user ?></a></td>
                                <?php endif;?>
                                <td><?php echo $pesanan->alamat ?></td>
                                <td><?php echo $pesanan->namaproduk ?></td>
                                <td><?php echo $pesanan->jumlah ?></td>
                                <td>
                                    <?php 
                                            switch ($pesanan->status) {
                                                case 1:
                                                    $pesanan->status = " 1 | Sudah dibayar, belum dikirim.";
                                                    break;
                                                case 2:
                                                    $pesanan->status = " 2 | Sudah dikirim.";
                                                    break;
                                                case 3:
                                                    $pesanan->status = " 3 | Sudah diterima.";
                                                    break;
                                                case 4:
                                                    $pesanan->status = " 4 | Pesanan selesai.";
                                                    break;
                                                case 5:
                                                    $pesanan->status = " 5 | Retur.";
                                                    break;
                                                default:
                                                    $pesanan->status = " 0 | Sudah order, belum dibayar.";
                                                    break;
                                            }
                                            echo $pesanan->status 
                                        ?>
                                </td>
                                <td>
                                    <?php if ($pesanan->bukti_transfer == null || $pesanan->bukti_transfer == ""){?>
                                        <b class="text-danger">Belum Diupload</b>
                                    <?php }else{?>
                                        <b class="text-success">Sudah Diupload</b><br>
                                        <a href="<?=base_url('assets/img/bukti_trf/').$pesanan->bukti_transfer;?>" target="__blank" class="btn btn-xs btn-success"><i class="fa fa-eye"></i> Lihat</a>
                                    <?php }?>
                                </td>
                                <td>
                                    <?php if ($pesanan->invoice == null){?>
                                        <b class="text-danger">Belum Diupload</b>
                                    <?php }else{?>
                                        <b class="text-success">Sudah Diupload</b><br>
                                        <a href="<?=base_url('assets/pdf/invoice/').$pesanan->invoice;?>" class="btn btn-xs btn-success"><i class="fa fa-download"></i> Download</a>
                                    <?php }?>
                                </td>
                                <td><?php echo $pesanan->created_at ?></td>
                                <?php if($user['level'] != "Customer"):?>
                                <td><?php echo $pesanan->modified_at ?></td>
                                <td>
                                    <a href="<?php echo site_url('pesanan/read/' . $pesanan->id ) ?>"
                                        class="btn btn-info"><i class="fa fa-eye"></i></a>
                                    <a href="<?php echo site_url('pesanan/update/' . $pesanan->id ) ?>"
                                        class="btn btn-warning"><i class="fa fa-edit"></i></a>
                                    <a data-href="<?php echo site_url('pesanan/delete/' . $pesanan->id ) ?>"
                                        class="btn btn-danger hapus-data"><i class="fa fa-trash"></i></a>
                                </td>
                                <?php else: ?>
                                <td>
                                    <?php if ($pesanan->status == 0){?>
                                    <a href="<?php echo site_url('pesanan/checkout/' . $pesanan->id ) ?>"
                                        class="btn btn-success"><i class="fas fa-shopping-cart"></i> Checkout</a><br><br>
                                    <a href="<?php echo site_url('pesanan/delete/' . $pesanan->id ) ?>"
                                        class="btn btn-danger hapus-data"><i class="fas fa-trash"></i> Batalkan</a>
                                    <?php }else if ($pesanan->status != 0 && $pesanan->status != 4 && $pesanan->status != 5){?>
                                        <a href="#" class="btn btn-warning"><i class="fas fa-clock"></i> Diproses</a>
                                    <?php }else{?>
                                        <a href="#" class="btn btn-info"><i class="fa fa-hashtag"></i> No Action</a>
                                    <?php }?>
                                </td>
                                <?php endif;?>
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
                        <?php if($user['level'] != "Customer"):?>
                        <?php echo anchor(site_url('pesanan/excel'), 'Excel', 'class="btn btn-primary"'); ?>
                        <?php endif;?>
                    </div>
                    <div class="col-md-6 text-right">
                        <?php echo $pagination ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>
$("#dt_pesanan").DataTable();
$(document).ready(function() {
    $(document).on("click", ".hapus-data", function() {
        hapus($(this).data("href"));
    });
});
</script>