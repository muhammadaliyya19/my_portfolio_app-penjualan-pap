<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="pull-left">
                    <div class="box-title">
                        <h4><?php echo $judul ?></h4>
                    </div>
                </div>
                <div class="pull-right">
                    <div class="box-title">
                        <a href="<?php echo base_url('pesanan') ?>" class="btn btn-primary"><i
                                class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        Pesanan anda telah kami terima. <br> Selanjutnya silakan transfer ke rekening BRI :
                        091234567890 sejumlah <b>Rp. <?=$total_harga;?>,-</b>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group <?php if(form_error('kode_pesanan')) echo 'has-error'?> ">
                                <label for="varchar">Kode Pesanan</label>
                                <input type="text" class="form-control" name="kode_pesanan" id="kode_pesanan"
                                    placeholder="Kode Pesanan" value="<?php echo $kode_pesanan; ?>" readonly />
                                <?php echo form_error('kode_pesanan', '<small style="color:red">','</small>') ?>
                            </div>
                            <div class="form-group <?php if(form_error('pembeli')) echo 'has-error'?> ">
                                <label for="int">Pembeli</label>
                                <input type="hidden" name="pembeli" value="<?php echo $pembeli; ?>" />
                                <input type="text" class="form-control" name="namapembeli" id="namapembeli"
                                    placeholder="Nama Pembeli" value="<?php echo $user['nama_user']; ?>" readonly />
                                <?php echo form_error('pembeli', '<small style="color:red">','</small>') ?>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group <?php if(form_error('id_produk')) echo 'has-error'?> ">
                                        <label for="int">Nama Produk / Harga</label>
                                        <!-- <input type="text" class="form-control" name="id_produk" id="id_produk" placeholder="Id Produk" value="<?php echo $id_produk; ?>" /> -->
                                        <input type="hidden" name="id_produk" value="<?php echo $id_produk; ?>"
                                            readonly />
                                        <input type="text" class="form-control" name="namaproduk" id="namaproduk"
                                            placeholder="Nama Produk"
                                            value="<?php echo $namaproduk . " / Rp. " . $harga . ",-"; ?>" readonly />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group <?php if(form_error('jumlah')) echo 'has-error'?> ">
                                        <label for="int">Jumlah</label>
                                        <input type="text" class="form-control" name="jumlah" id="jumlah"
                                            placeholder="Jumlah" value="<?php echo $jumlah; ?>" readonly />
                                        <?php echo form_error('jumlah', '<small style="color:red">','</small>') ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="int">Total Bayar</label>
                                <input type="text" class="form-control" name="total" id="total" placeholder="Total"
                                    value="<?php echo $total_harga; ?>" readonly />
                            </div>
                            <div class="form-group <?php if(form_error('invoice')) echo 'has-error'?> ">
                                <label class="form-label" for="invoice" style="overflow:hidden;">
                                    Upload Bukti Bayar (jpg, jpeg, png, pdf)
                                </label><br>                                
                                <input type="file" class="form-control" id="invoice" name="bukti_transfer" required>
                                <?php echo form_error('bukti_transfer', '<small style="color:red">','</small>') ?>
                            </div>
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <input type="hidden" name="status" value="1" />
                            <input type="hidden" name="q" value="checkout_user" />

                            <button type="submit" class="btn btn-primary btn-block">CHECKOUT DAN BAYAR</button>

                        </form>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
    </div>
</div>