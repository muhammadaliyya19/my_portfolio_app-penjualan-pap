<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="pull-left">
                    <div class="box-title">
                        <h4><?php echo $judul ?></h4>
                    </div>
                </div>
                <div class="pull-right">
                    <div class="box-title">
                        <a href="<?php echo base_url('pesanan') ?>" class="btn btn-primary"><i
                                class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group <?php if(form_error('kode_pesanan')) echo 'has-error'?> ">
                                <label for="varchar">Kode Pesanan</label>
                                <input type="text" class="form-control" name="kode_pesanan" id="kode_pesanan"
                                    placeholder="Kode Pesanan" value="<?php echo $kode_pesanan; ?>" readonly />
                                <?php echo form_error('kode_pesanan', '<small style="color:red">','</small>') ?>
                            </div>
                            <div class="form-group <?php if(form_error('pembeli')) echo 'has-error'?> ">
                                <label for="int">Pembeli</label>
                                <!-- <input type="text" class="form-control" name="pembeli" id="pembeli" placeholder="Pembeli" value="<?php echo $pembeli; ?>" /> -->
                                <select class="form-control" name="pembeli" id="pembeli" placeholder="Pembeli"
                                    <?=$button == "Update" ? "disabled" : "" ?>>
                                    <option value="">Pilih Pembeli / Customer</option>
                                    <?php foreach ($list_pembeli as $p): ?>
                                    <option value="<?=$p->id_user; ?>" <?=$pembeli == $p->id_user ? "selected" : ""; ?>>
                                        <?=$p->nama_user; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php echo form_error('pembeli', '<small style="color:red">','</small>') ?>
                            </div>
                            <div class="form-group <?php if(form_error('id_produk')) echo 'has-error'?> ">
                                <label for="int">Id Produk</label>
                                <!-- <input type="text" class="form-control" name="id_produk" id="id_produk" placeholder="Id Produk" value="<?php echo $id_produk; ?>" /> -->
                                <select class="form-control" name="id_produk" id="id_produk" placeholder="Produk"
                                    <?=$button == "Update" ? "disabled" : "" ?>>
                                    <option value="">Pilih Produk</option>
                                    <?php foreach ($list_produk_avail as $pa): ?>
                                    <option value="<?=$pa->id; ?>" <?=$id_produk == $pa->id ? "selected" : ""; ?>>Stok :
                                        <?=$pa->stok . " | " . $pa->nama; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php echo form_error('id_produk', '<small style="color:red">','</small>') ?>
                            </div>
                            <div class="form-group <?php if(form_error('jumlah')) echo 'has-error'?> ">
                                <label for="int">Jumlah</label>
                                <input type="text" class="form-control" name="jumlah" id="jumlah" placeholder="Jumlah"
                                    value="<?php echo $jumlah; ?>" <?=$button == "Update" ? "readonly" : "" ?> />
                                <?php echo form_error('jumlah', '<small style="color:red">','</small>') ?>
                            </div>

                            <?php if ($button == "Update"):?>

                            <div class="form-group <?php if(form_error('status')) echo 'has-error'?> ">
                                <label for="int">Status Pesanan</label>
                                <!-- <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php echo $status; ?>" /> -->

                                <select class="form-control" name="status" id="status" placeholder="Status Pesanan">
                                    <option value="0" <?=$status == 0 ? "selected" : ""; ?>>Sudah order belum dibayar.
                                    </option>
                                    <option value="1" <?=$status == 1 ? "selected" : ""; ?>>Sudah dibayar, belum
                                        dikiirm.</option>
                                    <option value="2" <?=$status == 2 ? "selected" : ""; ?>>Sedang dikirim, belum
                                        diterima pembeli.</option>
                                    <option value="3" <?=$status == 3 ? "selected" : ""; ?>>Sudah diterima pembeli.
                                    </option>
                                    <option value="4" <?=$status == 4 ? "selected" : ""; ?>>Pesanan selesai.</option>
                                    <option value="5" <?=$status == 5 ? "selected" : ""; ?>>Retur</option>
                                </select>
                                <?php echo form_error('status', '<small style="color:red">','</small>') ?>
                            </div>
                            <div class="form-group <?php if(form_error('invoice')) echo 'has-error'?> ">
                                <label class="form-label" for="invoice" style="overflow:hidden;">Invoice /
                                    Tagihan | <span class="<?=$invoice == null ? 'text-danger' : 'text-success' ?>">
                                        <?=$invoice == null ? "Belum diupload" : "Sudah diupload" ?>                                        
                                    </span>
                                </label><br>                                
                                <input type="file" class="form-control" id="invoice" name="invoice"
                                    <?=$button == "Update"? '' : 'required' ?>>
                                <?php echo form_error('invoice', '<small style="color:red">','</small>') ?>
                            </div>
                            <?php endif; ?>
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <button type="submit" class="btn btn-primary btn-block">SUBMIT</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).on('change', '.custom-file-input', function(e) {
    let fileName = $(this).val().split('\\').pop();
    $(this).next('.custom-file-label').addClass("selected").html(fileName);
    console.log("changed");
    let cout = $(this).data('cout');
    if (cout) {
        // console.log(cout);
        // console.log(e);
        var the_id = "ft_brg_" + cout;
        // console.log("#"+the_id);
        // console.log(event.target.files[0]);
        var output = document.getElementById(the_id);
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src); // free memory
        }
        // console.log("loaded");
    }
});
</script>