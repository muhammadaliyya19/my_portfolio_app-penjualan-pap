
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="pull-left">
                    <div class="box-title">
                        <h4><?php echo $judul ?></h4>
                    </div>
                </div>
                <div class="pull-right">
                    <div class="box-title">
                        <a href="<?php echo base_url('pesanan') ?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
               <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                   <table class="table">
                       <tr><td>Kode Pesanan</td><td><?php echo $kode_pesanan; ?></td></tr>
                       <tr><td>Pembeli</td><td><?php echo $pembeli; ?></td></tr>
                       <tr><td>Id Produk</td><td><?php echo $produk; ?></td></tr>
                       <tr><td>Jumlah</td><td><?php echo $jumlah; ?></td></tr>
                       <tr>
                        <?php 
                            switch ($status) {
                                case 1:
                                $status = " 1 | Sudah dibayar, belum dikirim.";
                                break;
                                case 2:
                                $status = " 2 | Sudah dikirim.";
                                break;
                                case 3:
                                $status = " 3 | Sudah diterima.";
                                break;
                                case 4:
                                $status = " 4 | Pesanan selesai.";
                                break;    
                                case 5:
                                $status = " 5 | Retur.";
                                break;
                                default:
                                $status = " 0 | Sudah order, belum dibayar.";
                                break;
                            }
                        ?>
                        <td>Status</td><td><?php echo $status; ?></td>
                    </tr>
                    <tr><td>Created At</td><td><?php echo $created_at; ?></td></tr>
                    <tr><td>Modified At</td><td><?php echo $modified_at; ?></td></tr>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
</div>