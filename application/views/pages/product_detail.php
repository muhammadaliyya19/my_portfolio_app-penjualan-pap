<div class="site-section" style="background-color: #eee; padding-top:35vh;">
    <div class="container">
        <div class="card">
            <div class="card-header text-center">
                <a href="#">
                    <h5 class="card-title font-weight-bold"><?=$nama;?></h5>
                </a>
            </div>
            <div class="card-body">
                <div class="row justify-content-center mb-4">
                    <div class="col-md-7">
                        <div class="testimonial-3-wrap bg-dark">
                            <div class="owl-carousel nonloop-block-13">
                                <div class="testimonial-3 d-flex justify-content-center">
                                    <div class="text-center">
                                        <img src="<?= base_url('assets/img/produk/').$foto; ?>"
                                            style="max-height: 300px; max-width: 100%;">
                                    </div>
                                </div>
                                <?php foreach ($detail as $key):?>
                                <div class="testimonial-3 d-flex justify-content-center">
                                    <div class="text-center">
                                        <img src="<?= base_url('assets/img/produk/').$key->path; ?>"
                                            style="max-height: 300px; max-width: 100%;">
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <div class="d-flex justify-content-between total font-weight-bold">
                                <span>Harga</span><span>Rp. <?=number_format($harga).",-";?></span>
                            </div>
                            <div class="d-flex justify-content-between font-weight-bold">
                                <span>Stok</span><span><?=$stok;?></span>
                            </div>
                            <div class="">
                                <div class="row">
                                    <div class="col-4">
                                        <span class="font-weight-bold">Keterangan</span><br>
                                    </div>
                                    <div class="col-8 text-right">
                                        <span><?=$keterangan;?></span>
                                    </div>
                                </div>
                            </div>
                            <?php if ($user != NULL) :?>
                            <div class="d-flex justify-content-end">
                                <form id="order_form" method="post" action="<?= site_url('pesanan/create_action') ?>" class="">
                                    <input type="hidden" name="q" value="user_order" />
                                    <input type="hidden" name="kode_pesanan" value="<?= $kode_pesanan; ?>" />
                                    <input type="hidden" name="id_produk" value="<?= $id; ?>" />
                                    <input type="hidden" name="pembeli" value="<?= $user['id_user']; ?>" />
                                    <div class="row">
                                        <div class="col-4">
                                            <span>Jumlah Pesan : </span>
                                        </div>
                                        <div class="col-8">
                                            <input type="number" id="jumlah_pesan" name="jumlah" style="width:100%;" required="">
                                        </div>
                                        <div class="col-12 text-center">
                                            <button type="button" class="btn btn-info btn-custom-1 mt-3" onclick="submitForm()"><i
                                                    class="fa fa-shopping-cart"></i>Masukan Keranjang</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <?php endif;?>
                        </div>
                        <!-- <div class="card-footer d-flex justify-content-between">
                            <a href="<?= ""; ?>" class="btn btn-info btn-custom-1">Keranjang</a>
                            <a href="<?= base_url('pesanan/create/'.$id); ?>"
                                class="btn btn-success btn-custom-1">Beli</a>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function submitForm() {
        let stok, jumlah_pesan;
        stok = <?= $stok; ?>;
        jumlah_pesan = document.getElementById("jumlah_pesan").value;
        if (jumlah_pesan == "" || jumlah_pesan == null || jumlah_pesan <= 0) {
            alert("Harap input jumlah dengan benar!")
        } else {
            if(jumlah_pesan > stok){
                alert("Jumlah pesanan melebihi stok !")
            }else{
                let text = "Masukkan produk ke keranjang?";
                if (confirm(text) == true) {
                    document.getElementById("order_form").submit();
                } else {
                    return false;
                }
            }
        }
    }
</script>