<div class="" style="">
  <div class="site-section overlay" style="background-image: url('<?= base_url("assets/third_party/images/");?>hero_1.jpg'); background-size:100%;">
    <div class="container">
      <div class="row align-items-center pt-5">

        <div class="col-md-5 pt-5 mt-5">
          <span class="text-cursive h5 text-red">Products</span>
          <h1 class="mb-3 font-weight-bold text-teal">Our Products</h1>
          <p><a href="index.html" class="text-dark">Home</a> <span class="mx-3">/</span> <strong>Products</strong></p>
        </div>

      </div>
    </div>
  </div>
</div>


  <div class="site-section" style="background-color: #eee;">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-12 mb-3">
          <form method="GET">
            <div class="row">
              <div class="col">
                <span>Nama Produk</span>
                <input type="text" class="form-control" placeholder="Nama Produk" name="nama" value="<?=$dt_filter == null ? '' : $dt_filter['nama'];?>">
              </div>
              <div class="col">
                <span>Harga Terendah</span>
                <input type="number" class="form-control" placeholder="Harga Terendah" name="min_price" value="<?=$dt_filter == null ? '' : $dt_filter['min_price'];?>">
              </div>
              <div class="col">
                <span>Harga Tertinggi</span>
                <input type="number" class="form-control" placeholder="Harga Tertinggi" name="max_price" value="<?=$dt_filter == null ? '' : $dt_filter['max_price'];?>">
              </div>
              <div class="col">
                <span>&nbsp</span>
                <input type="submit" class="form-control bg-info" value="Cari" style="background-color: #96d3ec!important;">
              </div>
              <div class="col">
                <span>&nbsp</span>
                <input type="button" class="form-control bg-info" value="Reset" style="background-color: #96d3ec!important;" onclick="window.location.href='<?= base_url('pages/products');?>';">
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="row">
        <?php 
          if(count($produk) > 0): 
            foreach ($produk as $p):?>
              <div class="col-md-6 col-lg-4">
                <div class="card shadow text-black">
                  <i class="fab fa-apple fa-lg pt-3 pb-1 px-3"></i>
                  <a href="<?=base_url('pages/products/'.$p->id);?>" target="_blank"><img src="<?=base_url('assets/img/produk/'.$p->foto);?>"
                    class="card-img-top px-2" alt="<?=$p->nama;?>" style="height: 175px;"/></a>
                  <div class="card-body">
                    <div class="text-center">
                      <a href="<?=base_url('pages/products/'.$p->id);?>" target="_blank"><h5 class="card-title"><?=$p->nama;?></h5></a>
                    </div>
                      <div class="d-flex justify-content-between font-weight-bold">
                        <span>Sisa stok</span><span><?=$p->stok;?></span>
                      </div>
                    <div class="d-flex justify-content-between total font-weight-bold">
                        <span>Harga</span><span>Rp. <?=number_format($p->harga).",-";?></span>
                    </div>
                    <div class="text-center mt-2">
                      <a class="btn btn-sm btn-info btn-custom-1" href="<?=base_url('pages/products/'.$p->id);?>" target="_blank">View Products</a>
                    </div>
                  </div>
                </div>
              </div>
        <?php endforeach;?>    
        <?php else:?>    
          <div class="col justify-content-center text-center mt-3">
            <h3>Produk tidak ditemukan!</h3>
          </div>
        <?php endif;?>    
      </div>
    </div>
  </div>