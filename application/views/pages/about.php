<div class="ftco-blocks-cover-1">
	<!-- data-stellar-background-ratio="0.5" style="background-image: url('images/hero_1.jpg')" -->
	<div class="site-section-cover overlay" data-stellar-background-ratio="0.5" style="background-image: url('<?= base_url("assets/third_party/images/");?>hero_1.jpg')">
		<div class="container">
			<div class="row align-items-center ">

				<div class="col-md-5 mt-5 pt-5">
					<span class="text-cursive h5 text-white">About</span>
					<h1 class="mb-3 font-weight-bold text-teal">About Us</h1>
					<p><a href="index.html" class="text-white">Home</a> <span class="mx-3">/</span> <strong>About</strong></p>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="site-section">
	<div class="container">
		<div class="row">
			<div class="col-md-6 pt-5 text-center">
				<img src="<?= base_url('assets/third_party/images/');?>img_1.jpg" alt="Image" class="img-fluid">
			</div>
			<div class="col-md-5 ml-auto pl-md-5">
				<span class="text-cursive h5 text-red">Tentang Kami</span>
				<h3 class="text-black">Sejarah Perusahaan</h3>
				<p align="justify">
					<span>
						PT. Paragon Abadi Prima didirikan pada tanggal – Juni 2011 yang beroperasi dan berkantor di Jl. Kapuk Kencana no. B5, Jakarta Utara. PT. Paragon Abadi Prima yang berlandaskan : semangat, komitmen, visi dan misi yang bulat dari para pendiri untuk mewujudkan partisipasi nyata dalam meningkatkan produktifitas dan penjualan pada PT. Paragon Abadi Prima. Pasar kami telah menjangkau seluruh Indonesia, dengan berbagai merek cotton bud, PT. Paragon Abadi Prima memiliki beberapa merek terdaftar seperti: Jasmine,Cherubs dan brand identity yang kuat di pasar Indonesia. Kami menjual produk kami di banyak supermarket nasional maupun toko rantai di seluruh indonesia, seperti Carrefour dan Lulu Hypermart. Kami adalah perusahaan dagang yang menjual cotton bud regular dan cotton bud baby dan kami mendistribusikan cotton bud regular dan cotton bud baby.
					</span>
				</p>
			</div>
		</div>
	</div>
</div>

<div class="site-section bg-teal">
	<div class="container">
		<div class="row mb-5">
			<div class="col-12 text-center">
				<span class="text-cursive h5 text-white d-block">Visi & Misi</span>
				<h2 class="text-white">Visi Misi Persahaan</h2>
			</div>
		</div>
		<div class="row justify-content-center text-center mb-5 section-2-title">	
			<div class="col-lg-6">
				<div class="package text-center bg-white">
					<span class="img-wrap"><img src="<?= base_url('assets/third_party/images/');?>flaticon/svg/002-target.svg" alt="Image" class="img-fluid"></span>
					<h3 class="text-teal">Visi</h3>
					<p>Visi perusahaan</p>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="package text-center bg-white">
					<span class="img-wrap"><img src="<?= base_url('assets/third_party/images/');?>flaticon/svg/001-jigsaw.svg" alt="Image" class="img-fluid"></span>
					<h3 class="text-success">Misi</h3>
					<p>Misi perusahaan Lorem ipsum dolor sit amet. Consequatur aliquam, fuga maiores amet quo corporis distinctio soluta recusandae?</p>
				</div>
			</div>			
		</div>
	</div>
</div>


<div class="site-section bg-info">
	<div class="container">
		<div class="row mb-5">
			<div class="col-12 text-center">
				<span class="text-cursive h5 text-white d-block">Organisasi Kami</span>
				<h2 class="text-white">Struktur Organisasi</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-12">
				<div class="package text-center bg-white">
					<img src="<?= base_url('assets/img/struktur_organisasi.png');?>" alt="struktur organisasi" class="img-fluid" width="900">
				</div>
			</div>
		</div>
	</div>
</div>



