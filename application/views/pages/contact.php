<div class="ftco-blocks-cover-1">
  <div class="site-section-cover overlay" data-stellar-background-ratio="0.5" style="background-image: url('<?= base_url("assets/third_party/images/");?>hero_1.jpg')">
    <div class="container">
      <div class="row align-items-center ">

        <div class="col-md-5 mt-5 pt-5">
          <span class="text-cursive h5 text-white">Contact</span>
          <h1 class="mb-3 font-weight-bold text-teal">Get In Touch</h1>
          <p><a href="index.html" class="text-white">Home</a> <span class="mx-3">/</span> <strong>Contact</strong></p>
        </div>
        
      </div>
    </div>
  </div>
</div>


<div class="site-section bg-light" id="contact-section">
  <div class="container">

    <!-- Address Section -->
    <div class="row justify-content-center text-center">
      <div class="">
        <span class="text-cursive h5 text-dark d-block">Kunjungi Kami</span>
        <h2>Alamat Kami</h2>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d701.2702465094217!2d106.76307513895007!3d-6.132908920711904!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6a1d7c78b7e785%3A0xdd2dd7f77fd32e1e!2sJl.%20Kapuk%20Kencana%2C%20RT.5%2FRW.3%2C%20Kapuk%20Muara%2C%20Kec.%20Penjaringan%2C%20Jkt%20Utara%2C%20Daerah%20Khusus%20Ibukota%20Jakarta!5e0!3m2!1sen!2sid!4v1676765843738!5m2!1sen!2sid" width="1200" height="500" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade" class="mt-2"></iframe>
      </div>
    </div>
    
    <!-- Contact Section -->    
  </div>
</div>

     <!-- Info Contact -->
     <div class="site-section bg-info">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <span class="text-cursive h5 text-white d-block">Tetap Terhubung</span>
            <h2 class="text-white">Kontak Kami</h2>
          </div>
        </div>
        <!-- Contact -->
        <div class="row justify-content-center text-center mb-5 section-2-title">	
          <div class="col-lg-4">
            <div class="package text-center bg-white">
              <span class="img-wrap"><img src="<?= base_url("assets/img/ico-telp.png");?>" alt="Image" class="img-fluid"></span>
              <h3 class="text-teal">Telepon</h3>
              <p><a href="https://wa.me/6285173208050">+6285173208050 (WA)</a></p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="package text-center bg-white">
              <span class="img-wrap"><img src="<?= base_url("assets/img/no_bg_ico_email.png");?>" alt="Image" class="img-fluid"></span>
              <h3 class="text-success">Email</h3>
              <p><a href="mailto:selvypisel@gmail.com?Subject=Kontak%20Paragon">selvypisel@gmail.com</a></p>
            </div>
          </div>			
          <div class="col-lg-4">
            <div class="package text-center bg-white">
              <span class="img-wrap"><img src="<?= base_url("assets/img/icon-internet.png");?>" alt="Image" class="img-fluid"></span>
              <h3 class="text-success">Website</h3>
              <p><a href="http://localhost/shelvy_TA" target="_blank">localhost/shelvy_TA</a></p>
            </div>
          </div>			
        </div>
          </div>
        </div>
      </div>
    </div>

  <div class="site-section">
    <div class="container">
      <div class="row mb-5">
        <div class="col-12 text-center">
          <span class="text-cursive h5 text-red d-block">Testimonial</span>
          <h2 class="text-black">What Our Client Says About Us</h2>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="testimonial-3-wrap">
            

            <div class="owl-carousel nonloop-block-13">
              <div class="testimonial-3 d-flex">
                <div class="vcard-wrap mr-5">
                  <img src="<?= base_url('assets/third_party/images/');?>person_1.jpg" alt="Image" class="vcard img-fluid">
                </div>
                <div class="text">
                  <h3>Jeff Woodland</h3>
                  <p class="position">Partner</p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam earum libero rem maxime magnam. Similique esse ab earum, autem consectetur.</p>
                </div>
              </div>

              <div class="testimonial-3 d-flex">
                <div class="vcard-wrap mr-5">
                  <img src="<?= base_url('assets/third_party/images/');?>person_3.jpg" alt="Image" class="vcard img-fluid">
                </div>
                <div class="text">
                  <h3>Jeff Woodland</h3>
                  <p class="position">Partner</p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam earum libero rem maxime magnam. Similique esse ab earum, autem consectetur.</p>
                </div>
              </div>

              <div class="testimonial-3 d-flex">
                <div class="vcard-wrap mr-5">
                  <img src="<?= base_url('assets/third_party/images/');?>person_2.jpg" alt="Image" class="vcard img-fluid">
                </div>
                <div class="text">
                  <h3>Jeff Woodland</h3>
                  <p class="position">Partner</p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam earum libero rem maxime magnam. Similique esse ab earum, autem consectetur.</p>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      
    </div>
  </div>