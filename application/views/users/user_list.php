
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="pull-left">
                    <div class="box-title">
                        <h4><?php echo $judul ?></h4>
                    </div>
                </div>
                <div class="pull-right">
                    <div class="box-title">
                        <a href="<?php echo base_url('users/create') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" width="100%" id="dt_user">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>E-mail</th>
                                <th>Alamat</th>
                                <th>Telepon</th>
                                <th>Jenis Kelamin</th>
                                <th>Role</th>
                                <th>Created At</th>
                                <th>Modified At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($users_data as $users){
                            ?>
                                <tr>
                                <td><?php echo ++$start ?></td>
                                <td><?php echo $users->nama_user ?></td>
                                <td><?php echo $users->email ?></td>
                                <td><?php echo $users->alamat ?></td>
                                <td><?php echo $users->telepon ?></td>
                                <td><?php echo $users->jk == 'L' ? 'Laki-laki' : 'Perempuan'; ?></td>
                                <td><?php echo $users->nama_role ?></td>
                                <td><?php echo $users->created_at ?></td>
                                <td><?php echo $users->modified_at ?></td><td>
                                    <a href="<?php echo site_url('users/read/' . $users->id_user ) ?>" class="btn btn-info"><i class="fa fa-eye"></i></a>
                                    <a href="<?php echo site_url('users/update/' . $users->id_user ) ?>" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                                    <a data-href="<?php echo site_url('users/delete/' . $users->id_user ) ?>" class="btn btn-danger hapus-data"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
                        <?php echo anchor(site_url('users/excel'), 'Excel', 'class="btn btn-primary"'); ?>

                    </div>
                    <div class="col-md-6 text-right">
                        <?php echo $pagination ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#dt_user").DataTable();
    $(document).ready(function() {
       $(document).on("click", ".hapus-data", function () {
          hapus($(this).data("href"));
      });
   });
</script>
