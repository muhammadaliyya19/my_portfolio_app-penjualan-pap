<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="pull-left">
                    <div class="box-title">
                        <h4><?php echo $judul ?></h4>
                    </div>
                </div>
                <div class="pull-right">
                    <div class="box-title">
                        <a href="<?php echo base_url('users') ?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <form action="<?php echo $action; ?>" method="post">
                         <div class="form-group <?php if(form_error('nama')) echo 'has-error'?> ">
                            <label for="varchar">Nama</label>
                            <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?php echo $nama; ?>" <?php echo $role=='4' ? 'readonly' : ''; ?>/>
                            <?php echo form_error('nama', '<small style="color:red">','</small>') ?>
                        </div>
                        <div class="form-group <?php if(form_error('email')) echo 'has-error'?> ">
                            <label for="varchar">Email</label>
                            <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" <?php echo $role=='4' ? 'readonly' : ''; ?>/>
                            <?php echo form_error('email', '<small style="color:red">','</small>') ?>
                        </div>
                        <div class="form-group <?php if(form_error('password')) echo 'has-error'?> ">
                            <label for="varchar">
                                Password 
                                <?php if ($method == "create"):?>
                                    <span class="text-success">(Default Password : 123456 )</span>
                                <?php else:?>
                                    <span class="text-success">( Isi Field Ini Untuk Melakukan Reset Password )</span>
                                <?php endif;?>
                            </label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="<?= $method == "create" ? "123456" : "" ?>" <?= $method == "create" ? "readonly" : "" ?>/>
                            <?php echo form_error('email', '<small style="color:red">','</small>') ?>
                        </div>
                        <div class="form-group <?php if(form_error('alamat')) echo 'has-error'?> ">
                            <label for="varchar">Alamat</label>
                            <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat" value="<?php echo $alamat; ?>"  <?php echo $role=='4' ? 'readonly' : ''; ?>/>
                            <?php echo form_error('alamat', '<small style="color:red">','</small>') ?>
                        </div>
                        <div class="form-group <?php if(form_error('telepon')) echo 'has-error'?> ">
                            <label for="varchar">No Telepon</label>
                            <input type="text" class="form-control" name="telepon" id="telepon" placeholder="No Telepon" value="<?php echo $telepon; ?>" <?php echo $role=='4' ? 'readonly' : ''; ?>/>
                            <?php echo form_error('email', '<small style="color:red">','</small>') ?>
                        </div>
                        <div class="form-group <?php if(form_error('jk')) echo 'has-error'?> ">
                            <label for="int">Jenis Kelamin</label>
                            <select class="form-control" id="jk" name="jk">
                                <option value=""> --- Pilih --- </option>
                                <option <?php echo $jk=='L' ? 'selected' : ''; ?> value="1">Laki-laki</option>
                                <option <?php echo $jk=='P' ? 'selected' : ''; ?> value="2">Perempuan</option>
                            </select>
                            <?php echo form_error('jk', '<small style="color:red">','</small>') ?>
                        </div>
                        <div class="form-group <?php if(form_error('role')) echo 'has-error'?> ">
                            <label for="int">Role</label>
                            <select class="form-control" id="role" name="role" <?php echo $role=='4' ? 'disabled' : ''; ?>>
                                <option value=""> --- Pilih --- </option>
                                <?php foreach ($roles as $r):?>
                                    <option <?php echo $role==$r->id_role ? 'selected' : ''; ?> value="<?=$r->id_role; ?>"><?=$r->nama_role; ?></option>
                                <?php endforeach;?>
                            </select>
                            <?php echo form_error('role', '<small style="color:red">','</small>') ?>
                        </div>
                        <div class="form-group <?php if(form_error('created_at')) echo 'has-error'?> ">
                            <label for="date">Created At</label>
                            <input type="text" class="form-control" name="created_at" id="created_at" placeholder="Created At" value="<?= $method == "create" ?  date('Y-m-d') : $created_at; ?>" readonly/>
                            <?php echo form_error('created_at', '<small style="color:red">','</small>') ?>
                        </div>
                        <div class="form-group <?php if(form_error('modified_at')) echo 'has-error'?> ">
                            <label for="date">Modified At</label>
                            <input type="text" class="form-control" name="modified_at" id="modified_at" placeholder="Modified At" value="<?= date('Y-m-d'); ?>" readonly/>
                            <?php echo form_error('modified_at', '<small style="color:red">','</small>') ?>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
                        <?php if ($role!='4'): ?>
                            <button type="submit" class="btn btn-primary btn-block">SUBMIT</button> 
                        <?php endif; ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>