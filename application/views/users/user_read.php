
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="pull-left">
                    <div class="box-title">
                        <h4><?php echo $judul ?></h4>
                    </div>
                </div>
                <div class="pull-right">
                    <div class="box-title">
                        <a href="<?php echo base_url('users') ?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
               <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                   <table class="table">
                       <tr><td>Nama</td><td><?php echo $nama; ?></td></tr>
                       <tr><td>E-mail</td><td><?php echo $email; ?></td></tr>
                       <tr><td>Alamat</td><td><?php echo $alamat; ?></td></tr>
                       <tr><td>Telepon</td><td><?php echo $telepon; ?></td></tr>
                       <tr><td>Jenis Kelamin</td><td><?php echo $jk == 'L' ? 'Laki-laki' : 'Perempuan'; ?></td></tr>
                       <tr><td>Role</td><td><?php echo $nama_role; ?></td></tr>
                       <tr><td>Created At</td><td><?php echo $created_at; ?></td></tr>
                       <tr><td>Modified At</td><td><?php echo $modified_at; ?></td></tr>
                   </table>
               </div>
           </div>
       </div>
   </div>
</div>
</div>