
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="pull-left">
                    <div class="box-title">
                        <h4><?php echo $judul ?></h4>
                    </div>
                </div>
                <div class="pull-right">
                    <div class="box-title">
                        <a href="<?php echo base_url('produk/create') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" width="100%" id="dt_produk">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Harga</th>
                                <th>Foto</th>
                                <th>Stok</th>
                                <th>Keterangan</th>
                                <th>Created At</th>
                                <th>Modified At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($produk_data as $produk)
                            {
                                ?>
                                <tr>
                                    <td><?php echo ++$start ?></td>
                                    <td><?php echo $produk->nama ?></td>
                                    <td><?php echo $produk->harga ?></td>
                                    <td><img src="<?php echo base_url("assets/img/produk/").$produk->foto; ?>" style="max-height: 200px; max-width: 100%;"></td>
                                    <td><?php echo $produk->stok ?></td>
                                    <td><?php echo $produk->keterangan ?></td>
                                    <td><?php echo $produk->created_at ?></td>
                                    <td><?php echo $produk->modified_at ?></td>
                                    <td>
                                        <a href="<?php echo site_url('produk/read/' . $produk->id ) ?>" class="btn btn-info"><i class="fa fa-eye"></i></a>
                                        <a href="<?php echo site_url('produk/update/' . $produk->id ) ?>" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                                        <a data-href="<?php echo site_url('produk/delete/' . $produk->id ) ?>" class="btn btn-danger hapus-data"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>                        
                    </table>
                </div>
                <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
		<?php echo anchor(site_url('produk/excel'), 'Excel', 'class="btn btn-primary"'); ?>
	    
                        </div>
                        <div class="col-md-6 text-right">
                            <?php echo $pagination ?>
                        </div>
                    </div>
                 
            </div>
        </div>
    </div>
</div>

<script>
    $("#dt_produk").DataTable();
    $(document).ready(function() {
       $(document).on("click", ".hapus-data", function () {
          hapus($(this).data("href"));
        });

    });
</script>
