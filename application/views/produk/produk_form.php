<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="pull-left">
                    <div class="box-title">
                        <h4><?php echo $judul ?></h4>
                    </div>
                </div>
                <div class="pull-right">
                    <div class="box-title">
                        <a href="<?php echo base_url('produk') ?>" class="btn btn-primary"><i
                                class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <form action="<?php echo $action;?>" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group <?php if(form_error('nama')) echo 'has-error'?> ">
                                <label for="varchar">Nama</label>
                                <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama"
                                    value="<?php echo $nama; ?>" />
                                <?php echo form_error('nama', '<small style="color:red">','</small>') ?>
                            </div>
                            <div class="form-group <?php if(form_error('harga')) echo 'has-error'?> ">
                                <label for="int">Harga</label>
                                <input type="text" class="form-control" name="harga" id="harga" placeholder="Harga"
                                    value="<?php echo $harga; ?>" />
                                <?php echo form_error('harga', '<small style="color:red">','</small>') ?>
                            </div>
                            <div class="form-group <?php if(form_error('foto')) echo 'has-error'?> ">
                                <label class="form-label" for="foto" style="overflow:hidden;">Foto Utama
                                    Produk</label><br>
                                <input type="file" class="form-control" id="foto" name="foto" onchange="loadFile(event)"
                                    <?=$method == "update"? '' : 'required' ?>>
                                <?php echo form_error('foto', '<small style="color:red">','</small>') ?>
                            </div>
                            <div class="card" style="height: 80%;">
                                <div class="card-body" align="center" style="height: auto;">
                                    <img src="<?= $method == "create" ? base_url('assets/img/holder_produk.png') : base_url('assets/img/produk/').$foto; ?>"
                                        style="max-height: 200px; max-width: 100%;" id="holderGaleri">
                                </div>
                            </div>
                            <div class="form-group <?php if(form_error('stok')) echo 'has-error'?> ">
                                <label for="int">Stok</label>
                                <input type="text" class="form-control" name="stok" id="stok" placeholder="Stok"
                                    value="<?php echo $stok; ?>" />
                                <?php echo form_error('stok', '<small style="color:red">','</small>') ?>
                            </div>
                            <div class="form-group <?php if(form_error('keterangan')) echo 'has-error'?> ">
                                <label for="keterangan">Keterangan</label>
                                <textarea class="form-control" rows="3" name="keterangan" id="keterangan"
                                    placeholder="Keterangan"><?php echo $keterangan; ?></textarea>
                                <?php echo form_error('keterangan', '<small style="color:red">','</small>') ?>
                            </div>
                            <div class="form-group <?php if(form_error('created_at')) echo 'has-error'?> ">
                                <label for="date">Created At</label>
                                <input type="text" class="form-control" name="created_at" id="created_at"
                                    placeholder="Created At"
                                    value="<?= $method == "create" ?  date('Y-m-d') : $created_at; ?>" readonly />
                                <?php echo form_error('created_at', '<small style="color:red">','</small>') ?>
                            </div>
                            <div class="form-group <?php if(form_error('modified_at')) echo 'has-error'?> ">
                                <label for="date">Modified At</label>
                                <input type="text" class="form-control" name="modified_at" id="modified_at"
                                    placeholder="Modified At" value="<?= date('Y-m-d'); ?>" readonly />
                                <?php echo form_error('modified_at', '<small style="color:red">','</small>') ?>
                            </div>
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                        </div>
                        <div class="col-md-4">
                            <div class="box-header">
                                <div class="pull-left">
                                    <div class="box-title">
                                        <h5>Detail Foto Produk <b>(Max 10)</b></h5>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <?php if ($method != "read"): ?>
                                    <div class="box-title">
                                        <button type="button" class="btn btn-primary btn-sm" id="tambah"><i
                                                class="fa fa-plus"></i> Detail Foto</button>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="keranjang">
                                <table class="table table-bordered" id="keranjang">
                                    <thead>
                                        <tr>
                                            <th width="85%">Foto</td>
                                            <th width="15%">Aksi</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if ($method != "create"): ?>
                                        <?php foreach ($detail as $key):?>
                                        <tr class="row-keranjang">
                                            <td align="center">
                                                <img src="<?= base_url('assets/img/produk/').$key->path; ?>"
                                                    style="max-height: 200px; max-width: 100%;" id="holderGaleri">
                                            </td>
                                            <?php if ($method == "update"): ?>
                                            <td class="aksi">
                                                <button type="button" class="btn btn-danger btn-sm"
                                                    id="tombol-hapus-detail" data-iddetail="<?=$key->id; ?>"><i
                                                        class="fa fa-trash"></i></button>
                                            </td>
                                            <?php else: ?>
                                            <td class="aksi">
                                                <button type="button" class="btn btn-success btn-sm"><i
                                                        class="fa fa-check"></i></button>
                                            </td>
                                            <?php endif; ?>
                                        </tr>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">SUBMIT</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Custom javascript -->
<script>
$(document).on('change', '.custom-file-input', function(e) {
    let fileName = $(this).val().split('\\').pop();
    $(this).next('.custom-file-label').addClass("selected").html(fileName);
    console.log("changed");
    let cout = $(this).data('cout');
    if (cout) {
        // console.log(cout);
        // console.log(e);
        var the_id = "ft_brg_" + cout;
        // console.log("#"+the_id);
        // console.log(event.target.files[0]);
        var output = document.getElementById(the_id);
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src); // free memory
        }
        // console.log("loaded");
    }
});

function loadFile(event) {
    console.log(event);
    var output = document.getElementById('holderGaleri');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
        URL.revokeObjectURL(output.src); // free memory
    }
};
var countBarang = 0;

$(document).ready(function() {
    $(document).on('click', '#tambah', function(e) {
        if (countBarang < 10) {
            countBarang++;
            $('table#keranjang tbody').append('<tr class="row-keranjang">' +
                '<td>' +
                '<div class="form-row">' +
                '<div class="custom-file">' +
                '<input type="hidden" name="counter[]" value="' + countBarang + '">' +
                '<input type="file" class="custom-file-input detal_galeri" id="foto" name="foto_detail[]" data-cout="' +
                countBarang + '">' +
                '</div>' +
                '</div>' +
                '<div class="thumbnail mt-2 imholder">' +
                '<img src="<?=base_url('assets/img/holder_produk.png'); ?>" id="ft_brg_' + countBarang +
                '" style="width:100%;">' +
                '</div>' +
                '</td>' +
                '<td class="aksi">' +
                '<button type="button" class="btn btn-danger btn-sm" id="tombol-hapus"><i class="fa fa-trash"></i></button>' +
                '</td>' +
                '</tr>');
            console.log(countBarang);
        } else {
            alert('Jumlah foto melebihi batas!')
        }
    })

    $(document).on('click', '#tombol-hapus', function() {
        $(this).closest('.row-keranjang').remove()
        countBarang--;
        console.log(countBarang);

        $('option[value="' + $(this).data('nama-barang') + '"]').show()

        if ($('tbody').children().length == 0) $('tfoot').hide()
    })

    $(document).on('click', '#tombol-hapus-detail', function() {
        var iddetail = $(this).data('iddetail');
        $(this).closest('.row-keranjang').remove();
        if ($('tbody').children().length == 0) $('tfoot').hide();
        var url = 'http://localhost/shelvy_TA/produk/delete_detail/' + iddetail;
        $.ajax({
            url: url,
            data: {
                iddetail: iddetail
            },
            method: 'GET',
            dataType: 'json',
            success: function(data) {
                console.log(data);
                if (!data) {} else {}
            }
        });
    });

    $(document).on('change', '.detal_galeri', function(e) {
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.custom-file-label').addClass("selected").html(fileName);
        console.log("changed");
        let cout = $(this).data('cout');
        if (cout) {
            console.log(cout);
            console.log(e);
            var the_id = "ft_brg_" + cout;
            console.log("#" + the_id);
            console.log(e.target.files[0]);
            var output = document.getElementById(the_id);
            output.src = URL.createObjectURL(e.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src); // free memory
            }
        }

    });
});
</script>