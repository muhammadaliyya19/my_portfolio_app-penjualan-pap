<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="pull-left">
                    <div class="box-title">
                        <h4><?php echo $judul ?></h4>
                    </div>
                </div>
                <div class="pull-right">
                    <div class="box-title">
                        <a href="<?php echo base_url('produk') ?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div>
                    <div class="row">
                        <div class="col-md-8">
                         <div class="form-group <?php if(form_error('nama')) echo 'has-error'?> ">
                            <label for="varchar">Nama</label>
                            <input type="text" class="form-control" value="<?php echo $nama; ?>" readonly />
                            <?php echo form_error('nama', '<small style="color:red">','</small>') ?>
                        </div>
                        <div class="form-group <?php if(form_error('harga')) echo 'has-error'?> ">
                            <label for="int">Harga</label>
                            <input class="form-control" readonly value="<?php echo $harga; ?>" />
                            <?php echo form_error('harga', '<small style="color:red">','</small>') ?>
                        </div>
                        <div class="form-group <?php if(form_error('foto')) echo 'has-error'?> ">
                            <label class="form-label" for="foto" style="overflow:hidden;">Foto Utama Produk</label><br>
                        </div>
                        <div class="card" style="height: 80%;">
                            <div class="card-body" align="center" style="height: auto;">
                                <img src="<?= base_url('assets/img/produk/').$foto; ?>" style="max-height: 200px; max-width: 100%;" id="holderGaleri">
                            </div>
                        </div>
                        <div class="form-group <?php if(form_error('stok')) echo 'has-error'?> ">
                            <label for="int">Stok</label>
                            <input type="text" class="form-control" readonly value="<?php echo $stok; ?>" />
                            <?php echo form_error('stok', '<small style="color:red">','</small>') ?>
                        </div>
                        <div class="form-group <?php if(form_error('keterangan')) echo 'has-error'?> ">
                            <label for="keterangan">Keterangan</label>
                            <textarea class="form-control" readonly rows="3" placeholder="Keterangan"><?php echo $keterangan; ?></textarea>
                            <?php echo form_error('keterangan', '<small style="color:red">','</small>') ?>
                        </div>
                        <div class="form-group <?php if(form_error('created_at')) echo 'has-error'?> ">
                            <label for="date">Created At</label>
                            <input type="text" class="form-control" name="created_at" id="created_at" placeholder="Created At" value="<?= $method == "create" ?  date('Y-m-d') : $created_at; ?>" readonly />
                            <?php echo form_error('created_at', '<small style="color:red">','</small>') ?>
                        </div>
                        <div class="form-group <?php if(form_error('modified_at')) echo 'has-error'?> ">
                            <label for="date">Modified At</label>
                            <input type="text" class="form-control" name="modified_at" id="modified_at" placeholder="Modified At" value="<?= $method == "read" ? $modified_at : date('Y-m-d'); ?>" readonly />
                            <?php echo form_error('modified_at', '<small style="color:red">','</small>') ?>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $id; ?>" />                         
                    </div>
                    <div class="col-md-4">
                        <div class="box-header">
                            <div class="pull-left">
                                <div class="box-title">
                                    <h5>Detail Foto Produk</h5>
                                </div>
                            </div>
                            <div class="pull-right">
                                <?php if ($method != "read"): ?>
                                    <div class="box-title">
                                        <button type="button" class="btn btn-primary btn-sm" id="tambah"><i class="fa fa-plus"></i> Detail Foto</button>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>                    
                        <div class="keranjang">
                            <table class="table table-bordered" id="keranjang">
                                <thead>
                                    <tr>
                                        <th width="100%">Foto</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($detail as $key):?>
                                            <tr class="row-keranjang">
                                                <td align="center">
                                                    <img src="<?= base_url('assets/img/produk/').$key->path; ?>" style="max-height: 200px; max-width: 100%;" id="holderGaleri">
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php if ($method != "read"): ?>
                            <button type="submit" class="btn btn-primary btn-block">SUBMIT</button> 
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Custom javascript -->
<script>        
    
</script>