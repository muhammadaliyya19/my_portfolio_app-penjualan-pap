<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="pull-left">
                    <div class="box-title">
                        <h4><?php echo $judul ?></h4>
                    </div>
                </div>
                <div class="pull-right">
                    <div class="box-title">
                        <a href="<?php echo base_url('barang_masuk/create') ?>" class="btn btn-primary"><i
                                class="fa fa-plus"></i> Tambah Data</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" width="100%" id="dt_bm">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Produk</th>
                                <th>Jumlah</th>
                                <th class="text-center">Status</th>
                                <th>Created At</th>
                                <th>Modified At</th>
                                <?php if($user['level'] == "Gudang" || $user['level'] == "Admin"):?>
                                <th>Action</th>
                                <?php endif;?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($barang_masuk_data as $barang_masuk)
                            {
                            ?>
                            <tr>
                                <td><?php echo ++$start ?></td>
                                <td><?php echo $barang_masuk->namaproduk ?></td>
                                <td><?php echo $barang_masuk->jumlah ?></td>
                                <td class="text-center">
                                    <?php if ($barang_masuk->status == 0) : ?>
                                    <span class="btn btn-warning">Menunggu Pengecekan Gudang</span>
                                    <?php elseif($barang_masuk->status == 1) : ?>
                                    <span class="btn btn-info">Lolos. Menunggu Persetujuan Admin</span>
                                    <?php elseif($barang_masuk->status == 2) : ?>
                                    <span class="btn btn-danger">Tidak Lolos Pengecekan</span>
                                    <?php else : ?>
                                    <span class="btn btn-success">Disetujui Admin</span>
                                    <?php endif; ?>
                                </td>
                                <td><?php echo $barang_masuk->created_at ?></td>
                                <td><?php echo $barang_masuk->modified_at ?></td>
                                <?php if($user['level'] == "Gudang" || $user['level'] == "Admin"):?>
                                <td>
                                    <?php if($user['level'] == "Gudang" && $barang_masuk->status == 0):?>
                                    
                                        <a href="<?php echo site_url('barang_masuk/lapor_cek/' . $barang_masuk->id ) ?>"
                                        class="btn btn-warning"><i class="fa fa-edit"></i> Lapor Pengecekan</a>
                                    
                                    <?php elseif($user['level'] == "Gudang" && $barang_masuk->status != 0):?>
                                        <a href="#" class="btn btn-info"><i class="fa fa-hashtag"></i> No Action</a>
                                    
                                    <?php endif;?>
                                    
                                    <?php if($user['level'] == "Admin" && $barang_masuk->status == 1):?>
                                    
                                        <a href="<?php echo site_url('barang_masuk/approve_cek/' . $barang_masuk->id ) ?>"
                                        class="btn btn-success"><i class="fa fa-check"></i> Approve</a>
                                    
                                    <?php elseif($user['level'] == "Admin" && ($barang_masuk->status == 3 || $barang_masuk->status == 0)):?>
                                        
                                        <a href="#" class="btn btn-info"><i class="fa fa-hashtag"></i> No Action</a>
                                    
                                    <?php endif;?>
                                </td>
                                <?php endif;?>
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>                        
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
                        <?php echo anchor(site_url('barang_masuk/excel'), 'Excel', 'class="btn btn-primary"'); ?>

                    </div>
                    <div class="col-md-6 text-right">
                        <?php echo $pagination ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$("#dt_bm").DataTable();
$(document).ready(function() {
    $(document).on("click", ".hapus-data", function() {
        hapus($(this).data("href"));
    });
});
</script>