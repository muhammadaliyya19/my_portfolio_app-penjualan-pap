<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="pull-left">
                    <div class="box-title">
                        <h4><?php echo $judul ?></h4>
                    </div>
                </div>
                <div class="pull-right">
                    <div class="box-title">
                        <a href="<?php echo base_url('barang_masuk') ?>" class="btn btn-primary"><i
                                class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <form action="<?php echo $action; ?>" method="post">
                            <div class="form-group <?php if(form_error('id_produk')) echo 'has-error'?> ">
                                <label for="int">Id Produk</label>
                                <!-- <input type="text" class="form-control" name="id_produk" id="id_produk" placeholder="Id Produk" value="<?php echo $id_produk; ?>" /> -->
                                <select class="form-control" name="id_produk" id="id_produk" placeholder="Produk"
                                    <?=$button == "Update" ? "disabled" : "" ?>>
                                    <option value="">Pilih Produk</option>
                                    <?php foreach ($list_produk as $pa): ?>
                                    <option value="<?=$pa->id; ?>" <?=$id_produk == $pa->id ? "selected" : ""; ?>>Stok :
                                        <?=$pa->stok . " | " . $pa->nama; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php echo form_error('id_produk', '<small style="color:red">','</small>') ?>
                            </div>
                            <div class="form-group <?php if(form_error('jumlah')) echo 'has-error'?> ">
                                <label for="int">Jumlah</label>
                                <input type="text" class="form-control" name="jumlah" id="jumlah" placeholder="Jumlah"
                                    value="<?php echo $jumlah; ?>" />
                                <?php echo form_error('jumlah', '<small style="color:red">','</small>') ?>
                            </div>
                            <input type="hidden" class="form-control" name="status" id="status" placeholder="Status"
                                value="0" />
                            <input type="hidden" class="form-control" name="created_at" id="created_at"
                                placeholder="Created At"
                                value="<?php echo $created_at == '' ? date('Y-m-d h:i:s') : $created_at; ?>" readonly />
                            <input type="hidden" class="form-control" name="modified_at" id="modified_at"
                                placeholder="Modified At"
                                value="<?php echo $modified_at == '' ? date('Y-m-d h:i:s') : $modified_at; ?>"
                                readonly />
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <button type="submit" class="btn btn-primary btn-block">SUBMIT</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>