<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="pull-left">
                    <div class="box-title">
                        <h4><?php echo $judul ?></h4>
                    </div>
                </div>
                <div class="pull-right">
                    <div class="box-title">
                        <a href="<?php echo base_url('barang_masuk') ?>" class="btn btn-primary"><i
                                class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <form action="<?php echo $action; ?>" method="post">
                            <div class="form-group <?php if(form_error('id_produk')) echo 'has-error'?> ">
                                <input type="hidden" name="id_produk" value="<?php echo $id_produk; ?>" />
                                <label for="int">Nama Produk</label>
                                <input type="text" class="form-control" name="nama_produk" id="nama_produk" placeholder="Nama Produk"
                                    value="<?php echo $namaproduk; ?>" readonly />
                                <?php echo form_error('jumlah', '<small style="color:red">','</small>') ?>
                            </div>
                            <div class="form-group <?php if(form_error('jumlah')) echo 'has-error'?> ">
                                <label for="int">Jumlah Dilaporkan Produksi</label>
                                <input type="text" class="form-control" name="jumlah" id="jumlah" placeholder="Jumlah"
                                    value="<?php echo $jumlah; ?>" readonly />
                                <?php echo form_error('jumlah', '<small style="color:red">','</small>') ?>
                            </div>
                            <div class="form-group <?php if(form_error('jumlah')) echo 'has-error'?> ">
                                <label for="int">Jumlah Aktual</label>
                                <input type="text" class="form-control" name="jumlah_cek" id="jumlah_cek" placeholder="Jumlah Pengecekan"/>
                                <?php echo form_error('jumlah', '<small style="color:red">','</small>') ?>
                            </div>
                            <div class="form-group <?php if(form_error('status')) echo 'has-error'?> ">
                                <label for="int">Status Pengecekan</label>
                                <!-- <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php echo $status; ?>" /> -->

                                <select class="form-control" name="status" id="status" placeholder="Status Pesanan">
                                    <option value="0" <?=$status == 0 ? "selected" : ""; ?>>Menunggu Pengecekan</option>
                                    <option value="1" <?=$status == 1 ? "selected" : ""; ?>>Jumlah Sesuai | Lolos Pengecekan</option>
                                    <option value="2" <?=$status == 2 ? "selected" : ""; ?>>Jumlah Tidak Sesuai | Tidak Lolos Pengecekan</option>
                                </select>
                                <?php echo form_error('status', '<small style="color:red">','</small>') ?>
                            </div>
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <button type="submit" class="btn btn-primary btn-block">SUBMIT</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>