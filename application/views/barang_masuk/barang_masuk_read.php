<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="pull-left">
                    <div class="box-title">
                        <h4><?php echo $judul ?></h4>
                    </div>
                </div>
                <div class="pull-right">
                    <div class="box-title">
                        <a href="<?php echo base_url('barang_masuk') ?>" class="btn btn-primary"><i
                                class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <table class="table">
                            <tr>
                                <td>NamaProduk</td>
                                <td><?php echo $namaproduk; ?></td>
                            </tr>
                            <tr>
                                <td>Jumlah</td>
                                <td><?php echo $jumlah; ?></td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>
                                    <?php if ($status == 0) : ?>
                                    <span class="btn btn-warning">Menunggu Pengecekan</span>
                                    <?php elseif($status == 1) : ?>
                                    <span class="btn btn-info">Lolos. Menunggu Persetujuan Admin</span>
                                    <?php elseif($status == 2) : ?>
                                    <span class="btn btn-danger">Tidak Lolos Pengecekan</span>
                                    <?php else : ?>
                                    <span class="btn btn-info">Disetujui Admin</span>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Created At</td>
                                <td><?php echo $created_at; ?></td>
                            </tr>
                            <tr>
                                <td>Modified At</td>
                                <td><?php echo $modified_at; ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>