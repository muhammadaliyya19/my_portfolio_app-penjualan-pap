<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="pull-left">
                    <div class="box-title">
                        <h4><?php echo $judul ?></h4>
                    </div>
                </div>
                <div class="pull-right">
                    <div class="box-title">
                        <a href="<?php echo base_url('cek_stok_gudang/create') ?>" class="btn btn-primary"><i
                                class="fa fa-plus"></i> Tambah Data</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" width="100%" id="dt_csg">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Produk</th>
                                <th>Sisa Stok (by system) / Jumlah Barang Masuk</th>
                                <th>Jumlah Cek (Aktual)</th>
                                <th>Jenis</th>
                                <th class="text-center">Status</th>
                                <th>Created At</th>
                                <th>Modified At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($cek_stok_gudang_data as $cek_stok_gudang)
                            {
                                ?>
                            <tr>
                                <td><?php echo ++$start ?></td>
                                <td><?php echo $cek_stok_gudang->namaproduk ?></td>
                                <td class="text-right"><?php echo $cek_stok_gudang->jml_brg_masuk == NULL ? $cek_stok_gudang->stok_sekarang : $cek_stok_gudang->jml_brg_masuk;  ?></td>
                                <td class="text-right"><?php echo $cek_stok_gudang->jumlah_cek ?></td>
                                <td><?php echo $cek_stok_gudang->jenis ?></td>
                                <td class="text-center">
                                    <?php if($cek_stok_gudang->status == 1) : ?>
                                    <span class="btn btn-info">Lolos. Menunggu Persetujuan Admin</span>
                                        <?php if($user['level'] == 'Admin' && $cek_stok_gudang->jenis == "CEK BARANG MASUK") : ?>
                                            <br><a href="<?php echo site_url('barang_masuk'); ?>" class="text-primary" target="_blank">Setujui via barang masuk</a>
                                        <?php endif; ?>                                    
                                    <?php elseif($cek_stok_gudang->status == 2) : ?>
                                    <span class="btn btn-danger">Tidak Lolos Pengecekan</span>
                                    <?php elseif($cek_stok_gudang->status == 0) : ?>
                                    <span class="btn btn-warning">Menunggu Pengecekan Gudang</span>
                                    <?php else : ?>
                                    <span class="btn btn-success">Disetujui Admin</span>
                                    <?php endif; ?>
                                </td>
                                <td><?php echo $cek_stok_gudang->created_at ?></td>
                                <td><?php echo $cek_stok_gudang->modified_at ?></td>
                                <td>
                                <?php if($cek_stok_gudang->jenis == "CEK BARANG MASUK"):?>
                                        <a href="#" class="btn btn-info"><i class="fa fa-hashtag"></i> No Action</a>                                
                                <?php else:?>
                                    <?php if($cek_stok_gudang->status == 0 && $user['level'] == "Gudang"):?>
                                            <a href="<?php echo site_url('cek_stok_gudang/update/' . $cek_stok_gudang->id ) ?>"
                                                class="btn btn-warning"><i class="fa fa-edit"></i> Lapor Pengecekan</a>                                        
                                    <?php else:?>
                                        <?php if($user['level'] == "Admin" && $cek_stok_gudang->status == 1):?>
                                            <a href="<?php echo site_url('cek_stok_gudang/approve_cek/' . $cek_stok_gudang->id ) ?>" class="btn btn-success"><i class="fa fa-check"></i> Approve</a>
                                        <?php else:?>
                                                <a href="#" class="btn btn-info"><i class="fa fa-hashtag"></i> No Action</a>                                
                                        <?php endif;?>
                                    <?php endif;?>
                                <?php endif;?>
                                </td>
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
                        <?php echo anchor(site_url('cek_stok_gudang/excel'), 'Excel', 'class="btn btn-primary"'); ?>

                    </div>
                    <div class="col-md-6 text-right">
                        <?php echo $pagination ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>
$("#dt_csg").DataTable();
$(document).ready(function() {
    $(document).on("click", ".hapus-data", function() {
        hapus($(this).data("href"));
    });
});
</script>