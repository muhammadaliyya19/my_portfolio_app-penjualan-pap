<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="pull-left">
                    <div class="box-title">
                        <h4><?php echo $judul ?></h4>
                    </div>
                </div>
                <div class="pull-right">
                    <div class="box-title">
                        <a href="<?php echo base_url('cek_stok_gudang') ?>" class="btn btn-primary"><i
                                class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <form action="<?php echo $action; ?>" method="post">
                            <div class="form-group <?php if(form_error('jenis')) echo 'has-error'?> ">
                                <label for="varchar">Jenis</label>
                                <input type="text" class="form-control" name="jenis" id="jenis" placeholder="Jenis"
                                    value="<?php echo $jenis == "" ? "PERMINTAAN CEK STOK" : $jenis; ?>" readonly />
                                <?php echo form_error('jenis', '<small style="color:red">','</small>') ?>
                            </div>
                            <div class="form-group <?php if(form_error('id_produk')) echo 'has-error'?> ">
                                <label for="int">Produk</label>
                                <!-- <input type="text" class="form-control" name="id_produk" id="id_produk"
                                    placeholder="Id Produk" value="<?php echo $id_produk; ?>" /> -->
                                    <!-- <?php var_dump($list_produk) ?> -->
                                <select class="form-control" name="id_produk" id="id_produk" placeholder="Produk"
                                    <?=$button == "Update" ? "disabled" : "" ?>>
                                    <option value="">Pilih Produk</option>

                                    <?php foreach ($list_produk as $pa): ?>
                                    <option value="<?=$pa->id; ?>" <?=$id_produk == $pa->id ? "selected" : ""; ?>>Stok Berdasar Sistem :
                                        <?=$pa->stok . " | " . $pa->nama; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php echo form_error('id_produk', '<small style="color:red">','</small>') ?>
                            </div>
                            <?php if ($button != 'Create'): ?>
                            <div class="form-group <?php if(form_error('jumlah_cek')) echo 'has-error'?> ">
                                <label for="int">Jumlah Cek Aktual</label>
                                <input type="text" class="form-control" name="jumlah_cek" id="jumlah_cek"
                                    placeholder="Jumlah Cek" value="<?php echo $jumlah_cek; ?>" />
                                <?php echo form_error('jumlah_cek', '<small style="color:red">','</small>') ?>
                            </div>
                            <div class="form-group <?php if(form_error('status')) echo 'has-error'?> ">
                                <label for="int">Status Pengecekan</label>
                                <!-- <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php echo $status; ?>" /> -->

                                <select class="form-control" name="status" id="status" placeholder="Status Pesanan">
                                    <option value="0" <?=$status == 0 ? "selected" : ""; ?>>Menunggu Pengecekan Gudang
                                    </option>
                                    <option value="1" <?=$status == 1 ? "selected" : ""; ?>>Jumlah Sesuai | Lolos
                                        Pengecekan</option>
                                    <option value="2" <?=$status == 2 ? "selected" : ""; ?>>Jumlah Tidak Sesuai | Tidak
                                        Lolos Pengecekan</option>
                                </select>
                                <?php echo form_error('status', '<small style="color:red">','</small>') ?>
                            </div>
                            <?php endif; ?>
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <button type="submit" class="btn btn-primary btn-block">SUBMIT</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>