<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Produk_model extends CI_Model
{

    public $table = 'produk';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function get_avail_prod()
    {
        $this->db->where('stok >', 0);
        $param_search = isset($_GET) ? $_GET : null;
        if ($param_search != null) {
            // var_dump($param_search); 
            if ($param_search['nama'] != '') {
                $this->db->like('nama', $param_search['nama']);
            }
            if ($param_search['min_price'] != '') {
                $this->db->where('harga >=', (int)$param_search['min_price']);
            }
            if ($param_search['max_price'] != '') {
                $this->db->where('harga <=', (int)$param_search['max_price']);
            }
            $res = $this->db->get($this->table)->result();
            // echo $this->db->last_query(); 
            // return $this->db->get($this->table)->result();
            return $res;
        }else{
            return $this->db->limit(20, 0)->get($this->table)->result();
        }
    }

    // get data detail by id produk
    function get_detail_by_produk($id)
    {
        $this->db->where("id_produk", $id);
        return $this->db->get("detil_produk")->result();
    }

    // get data detail by id produk
    function get_detail_by_id($id)
    {
        $this->db->where("id", $id);
        return $this->db->get("detil_produk")->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
        $this->db->or_like('nama', $q);
        $this->db->or_like('harga', $q);
        $this->db->or_like('foto', $q);
        $this->db->or_like('stok', $q);
        $this->db->or_like('keterangan', $q);
        $this->db->or_like('created_at', $q);
        $this->db->or_like('modified_at', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
        $this->db->or_like('nama', $q);
        $this->db->or_like('harga', $q);
        $this->db->or_like('foto', $q);
        $this->db->or_like('stok', $q);
        $this->db->or_like('keterangan', $q);
        $this->db->or_like('created_at', $q);
        $this->db->or_like('modified_at', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // update data
    function update_stok($id, $trans, $sum)
    {
        $this->db->where($this->id, $id);
        $currentStok = (int)$this->db->select('stok')->get($this->table)->row()->stok;
        switch ($trans) {
            case 'tambah':
                $currentStok += (int)$sum;
                break;
            case 'kurang':
                $currentStok -= (int)$sum;
                break;
            case 'ubah':
                $currentStok = (int)$sum;
                break;
        }
        $data = ['stok' => $currentStok];
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    // delete data detail - batch
    function delete_detail_batch($id_produk)
    {
        $this->db->where("id_produk", $id_produk);
        $this->db->delete("detil_produk");
        return 1;
    }

    // delete data detail - single
    function delete_detail($id_detail)
    {
        $this->db->where("id", $id_detail);
        $this->db->delete("detil_produk");
        return 1;
    }

    // PENGELOLAAN DETAIL GALERI
    public function tambah_detail_foto($data){
        $lastProduk = $this->getLastProduk();           
        for ($i=0; $i < count($data); $i++) { 
            if (isset($_POST['id']) && $_POST['id'] != "") {
                $id = $this->input->post('id', true);
                $data[$i] += array('id_produk'=>$id);
            }else{
                $data[$i] += array('id_produk'=>$lastProduk['id']);                
            }
        }
        return $this->db->insert_batch('detil_produk', $data);
    }

    public function getLastProduk()
    {
        $row = $this->db->select("*")->limit(1)->order_by('id',"DESC")->get("produk")->row_array();
        return $row;
    }

}

/* End of file Produk_model.php */
/* Location: ./application/models/Produk_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2022-05-10 15:13:17 */
/* http://harviacode.com */