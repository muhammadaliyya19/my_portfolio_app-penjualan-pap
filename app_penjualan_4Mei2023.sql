-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 04 Bulan Mei 2023 pada 10.37
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `app_penjualan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `id` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `jumlah` int(4) NOT NULL,
  `status` int(3) NOT NULL,
  `created_at` date NOT NULL,
  `modified_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `barang_masuk`
--

INSERT INTO `barang_masuk` (`id`, `id_produk`, `jumlah`, `status`, `created_at`, `modified_at`) VALUES
(1, 32, 7, 3, '2022-11-04', '2022-11-04'),
(2, 31, 3, 3, '2022-11-19', '2022-11-19'),
(3, 31, 2, 3, '2022-11-19', '2022-11-19'),
(4, 32, 2, 1, '2022-12-05', '2022-12-05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cek_stok_gudang`
--

CREATE TABLE `cek_stok_gudang` (
  `id` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `id_barang_masuk` int(11) DEFAULT NULL,
  `stok_sekarang` int(11) DEFAULT NULL,
  `jumlah_cek` int(4) NOT NULL,
  `jenis` varchar(20) DEFAULT NULL,
  `status` int(3) NOT NULL,
  `created_at` date NOT NULL,
  `modified_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `cek_stok_gudang`
--

INSERT INTO `cek_stok_gudang` (`id`, `id_produk`, `id_barang_masuk`, `stok_sekarang`, `jumlah_cek`, `jenis`, `status`, `created_at`, `modified_at`) VALUES
(1, 32, 1, NULL, 7, 'CEK BARANG MASUK', 3, '2022-11-04', '2022-11-04'),
(2, 31, 2, NULL, 3, 'CEK BARANG MASUK', 3, '2022-11-19', '2022-11-19'),
(3, 24, NULL, 12, 12, 'PERMINTAAN CEK STOK', 3, '2022-11-19', '2022-11-19'),
(4, 31, 3, NULL, 2, 'CEK BARANG MASUK', 3, '2022-11-19', '2022-11-19'),
(5, 32, 4, NULL, 2, 'CEK BARANG MASUK', 1, '2022-12-05', '2022-12-05'),
(7, 24, NULL, 12, 12, 'PERMINTAAN CEK STOK', 3, '2022-12-05', '2022-12-11'),
(8, 0, NULL, NULL, 0, 'PERMINTAAN CEK STOK', 0, '2022-12-11', '2022-12-11'),
(9, 0, NULL, NULL, 0, 'PERMINTAAN CEK STOK', 0, '2022-12-11', '2022-12-11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detil_produk`
--

CREATE TABLE `detil_produk` (
  `id` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `path` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `detil_produk`
--

INSERT INTO `detil_produk` (`id`, `id_produk`, `path`) VALUES
(17, 24, '1659773251-2022-08-061007310.jpg'),
(18, 24, '1659773251-2022-08-061007311.jpg'),
(29, 31, '1659775530-2022-08-061045300.jpg'),
(30, 31, '1659775530-2022-08-061045301.jpg'),
(31, 32, '1659844732-2022-08-070558520.jpg'),
(32, 32, '1659844732-2022-08-070558521.jpg'),
(34, 32, '1659844832-2022-08-070600320.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengaturan`
--

CREATE TABLE `pengaturan` (
  `id_pengaturan` int(11) NOT NULL,
  `nama_aplikasi` varchar(255) NOT NULL,
  `logo` varchar(128) NOT NULL,
  `smtp_host` varchar(128) NOT NULL,
  `smtp_email` varchar(128) NOT NULL,
  `smtp_username` varchar(128) NOT NULL,
  `smtp_password` varchar(128) NOT NULL,
  `smtp_port` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pengaturan`
--

INSERT INTO `pengaturan` (`id_pengaturan`, `nama_aplikasi`, `logo`, `smtp_host`, `smtp_email`, `smtp_username`, `smtp_password`, `smtp_port`) VALUES
(1, 'My App', 'layers.png', 'ssl://smtp.gmail.com', 'smtp.email456@gmail.com', 'smtp.email456@gmail.com', 'adminsmtp123', 465);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pesanan`
--

CREATE TABLE `pesanan` (
  `id` int(11) NOT NULL,
  `kode_pesanan` varchar(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `jumlah` int(4) NOT NULL,
  `status` int(3) NOT NULL COMMENT '0 : Sudah order, belum bayar\r\n1 : Sudah bayar, belum dikirim\r\n2 : Sudah dikirim\r\n3 : Sdah diterima\r\n4 : Pesanan selesai\r\n5 : Retur',
  `invoice` varchar(40) DEFAULT NULL,
  `created_at` date NOT NULL,
  `modified_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pesanan`
--

INSERT INTO `pesanan` (`id`, `kode_pesanan`, `pembeli`, `id_produk`, `jumlah`, `status`, `invoice`, `created_at`, `modified_at`) VALUES
(1, 'X3RUZS20221', 4, 32, 1, 4, 'X3RUZS20221-2022-11-07-071859.pdf', '2022-11-04', '2022-11-07'),
(2, 'DXNA4Z20221', 4, 32, 1, 2, 'DXNA4Z20221-2022-11-07-071827.pdf', '2022-11-07', '2022-11-07'),
(3, '2WTHDM20221', 4, 24, 1, 0, NULL, '2022-11-19', '2022-11-19'),
(4, 'JP6Z2G20221', 3, 24, 1, 1, NULL, '2022-12-10', '2022-12-11'),
(6, 'RLNG6A20230', 4, 24, 13, 0, NULL, '2023-01-02', '2023-01-02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `nama` varchar(48) NOT NULL,
  `harga` int(8) NOT NULL,
  `foto` varchar(128) NOT NULL,
  `stok` int(4) NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` date NOT NULL DEFAULT current_timestamp(),
  `modified_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`id`, `nama`, `harga`, `foto`, `stok`, `keterangan`, `created_at`, `modified_at`) VALUES
(24, 'Tester kedua', 1231, '1659773251-2022-08-06-100731.jpg', 12, 'PC', '2022-08-06', '2022-11-04'),
(31, 'Laptop ASUS x540L', 2790000, '1659774410-2022-08-06-102650.jpg', 10, 'OK minus baterai saja, drop, lainnya aman.', '2022-08-06', '2022-11-04'),
(32, 'LCD Proyektor  Cheerlux', 750000, '1659844732-2022-08-07-055852.jpg', 8, 'Fullset,  second like new, mulus. Cocok untuk penggunaan pribadi, seperti family theater.', '2022-08-07', '2022-11-04');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role`
--

CREATE TABLE `role` (
  `id_role` int(11) NOT NULL,
  `nama_role` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `role`
--

INSERT INTO `role` (`id_role`, `nama_role`) VALUES
(1, 'Admin'),
(2, 'Produksi'),
(3, 'Gudang'),
(4, 'Customer');

-- --------------------------------------------------------

--
-- Struktur dari tabel `token_user`
--

CREATE TABLE `token_user` (
  `id` int(11) NOT NULL,
  `id_user` char(10) NOT NULL,
  `tgl` timestamp NOT NULL DEFAULT current_timestamp(),
  `token` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `token_user`
--

INSERT INTO `token_user` (`id`, `id_user`, `tgl`, `token`) VALUES
(1, '6', '2022-11-19 01:58:32', 'H5E7LBCR94I1A65N6MADLOOEN6Q9K21L'),
(2, '6', '2022-11-19 02:12:36', 'QX7SB1EU3TQM2B17YJKNHHPOYBMG7TUQ'),
(3, '1', '2023-04-05 03:18:17', 'AZO9TENT8KZE7ZQZG86UIHUU2MPIJQ85'),
(4, '1', '2023-04-05 03:30:04', '04YKMQO62PD9H9OWIHMZAIYUGL17H6ES');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(10) NOT NULL,
  `nama_user` varchar(128) NOT NULL,
  `alamat` varchar(128) NOT NULL,
  `jk` enum('L','P') NOT NULL,
  `telepon` char(12) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(128) NOT NULL,
  `gambar` varchar(128) NOT NULL,
  `id_role` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `modified_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `nama_user`, `alamat`, `jk`, `telepon`, `email`, `password`, `gambar`, `id_role`, `created_at`, `modified_at`) VALUES
(1, 'PRODUKSI', 'Dsn. Kebondalem - Kandangan', 'L', '085784114468', 'produksi@gmail.com', '$2y$10$FcERMR1U6KDWdPPGcTllgOnbsFTXMD4aFsljDeo.K7wvU.jSsBqEa', 'Screenshot_(29).png', 2, '2022-08-07', '2022-08-07'),
(2, 'SUPERADMIN', 'BANDUNG', 'L', '083822623170', 'superadmin@admin.com', '$2y$10$t2LIGNkyTgoo.wfFq65HU.RMH3.maKSCVMYL1.ix0l.xZjAOfi1PK', 'man-1.png', 1, '2022-06-01', '2022-08-01'),
(3, 'ADMINISTRATOR', 'Batulawang', 'L', '085864273000', 'admin@admin.com', '$2y$10$t2LIGNkyTgoo.wfFq65HU.RMH3.maKSCVMYL1.ix0l.xZjAOfi1PK', 'lg_lcd.png', 1, '2022-06-01', '2022-08-07'),
(4, 'User 001', 'Batulawang', 'L', '085864273756', 'user@user.com', '$2y$10$betx1MwS5SC/UacmH8eYgO/zPr2F1848nrAqgPB8Zw65jW4nDdmTS', 'PAP-removebg-preview.png', 4, '2022-06-01', '2022-08-01'),
(6, 'GUDANG', 'Jombang', 'L', '0354328351', 'gudang@gmail.com', '$2y$10$/IE/U/cyw0w9POHggHyNiOQ0U65zeU0k3ILGQ7pSIuJNOtlz0YHt6', 'Ajq2J3.jpg', 3, '2022-08-07', '2022-08-07');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `cek_stok_gudang`
--
ALTER TABLE `cek_stok_gudang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `detil_produk`
--
ALTER TABLE `detil_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`id_pengaturan`);

--
-- Indeks untuk tabel `pesanan`
--
ALTER TABLE `pesanan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indeks untuk tabel `token_user`
--
ALTER TABLE `token_user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `barang_masuk`
--
ALTER TABLE `barang_masuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `cek_stok_gudang`
--
ALTER TABLE `cek_stok_gudang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `detil_produk`
--
ALTER TABLE `detil_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT untuk tabel `pengaturan`
--
ALTER TABLE `pengaturan`
  MODIFY `id_pengaturan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `pesanan`
--
ALTER TABLE `pesanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT untuk tabel `role`
--
ALTER TABLE `role`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `token_user`
--
ALTER TABLE `token_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
