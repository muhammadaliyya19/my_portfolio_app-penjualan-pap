-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 08 Jul 2023 pada 17.57
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_penjualan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth`
--

CREATE TABLE `auth` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `id` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `jumlah` int(4) NOT NULL,
  `status` int(3) NOT NULL,
  `created_at` date NOT NULL,
  `modified_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `barang_masuk`
--

INSERT INTO `barang_masuk` (`id`, `id_produk`, `jumlah`, `status`, `created_at`, `modified_at`) VALUES
(1, 36, 5, 3, '2023-05-19', '2023-05-19'),
(2, 35, 8, 2, '2023-05-19', '2023-05-19'),
(3, 36, 1, 3, '2023-05-19', '2023-05-19'),
(4, 36, 5, 3, '2023-05-23', '2023-05-23'),
(5, 35, 6, 2, '2023-05-23', '2023-05-23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang_masuk_model`
--

CREATE TABLE `barang_masuk_model` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cek_stok_gudang`
--

CREATE TABLE `cek_stok_gudang` (
  `id` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `id_barang_masuk` int(11) DEFAULT NULL,
  `stok_sekarang` int(11) DEFAULT NULL,
  `jumlah_cek` int(4) NOT NULL,
  `jenis` varchar(20) DEFAULT NULL,
  `status` int(3) NOT NULL,
  `created_at` date NOT NULL,
  `modified_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `cek_stok_gudang`
--

INSERT INTO `cek_stok_gudang` (`id`, `id_produk`, `id_barang_masuk`, `stok_sekarang`, `jumlah_cek`, `jenis`, `status`, `created_at`, `modified_at`) VALUES
(3, 35, NULL, 7, 10, 'PERMINTAAN CEK STOK', 2, '2023-05-19', '2023-05-19'),
(4, 36, 1, NULL, 5, 'CEK BARANG MASUK', 3, '2023-05-19', '2023-05-19'),
(5, 35, 2, NULL, 7, 'CEK BARANG MASUK', 2, '2023-05-19', '2023-05-19'),
(6, 36, 3, NULL, 1, 'CEK BARANG MASUK', 3, '2023-05-19', '2023-05-19'),
(7, 36, 4, NULL, 6, 'CEK BARANG MASUK', 3, '2023-05-23', '2023-05-23'),
(8, 35, 5, NULL, 8, 'CEK BARANG MASUK', 2, '2023-05-23', '2023-05-23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cek_stok_gudang_model`
--

CREATE TABLE `cek_stok_gudang_model` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dashboard`
--

CREATE TABLE `dashboard` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `detil_produk`
--

CREATE TABLE `detil_produk` (
  `id` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `path` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `detil_produk`
--

INSERT INTO `detil_produk` (`id`, `id_produk`, `path`) VALUES
(39, 35, '1684501461-2023-05-190304210.jpg'),
(40, 36, '1684501531-2023-05-190305310.jpg'),
(42, 38, '1686847759-2023-06-150649191.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pesanan`
--

CREATE TABLE `pesanan` (
  `id` int(11) NOT NULL,
  `kode_pesanan` varchar(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `jumlah` int(4) NOT NULL,
  `status` int(3) NOT NULL COMMENT '0 : Sudah order, belum bayar\r\n1 : Sudah bayar, belum dikirim\r\n2 : Sudah dikirim\r\n3 : Sdah diterima\r\n4 : Pesanan selesai\r\n5 : Retur',
  `bukti_transfer` text DEFAULT NULL,
  `invoice` varchar(40) DEFAULT NULL,
  `created_at` date NOT NULL,
  `modified_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pesanan`
--

INSERT INTO `pesanan` (`id`, `kode_pesanan`, `pembeli`, `id_produk`, `jumlah`, `status`, `bukti_transfer`, `invoice`, `created_at`, `modified_at`) VALUES
(2, '2Y38WW20230', 7, 35, 1, 2, '', 'Inv_2Y38WW20230_2023_05_19_035620.pdf', '2023-05-19', '2023-05-19'),
(3, 'E8NTS620230', 4, 35, 2, 2, '', 'Inv_E8NTS620230_2023_05_19_035653.pdf', '2023-05-19', '2023-05-19'),
(4, 'OF1WQP20230', 4, 36, 1, 2, '', 'Inv_Inv_OF1WQP20230_2023_05_23_025137.pd', '2023-05-23', '2023-06-16'),
(5, 'WTW5D620230', 4, 35, 2, 1, 'Trf_WTW5D620230_2023_07_08_055029.png', NULL, '2023-07-08', '2023-07-08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pesanan_model`
--

CREATE TABLE `pesanan_model` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `nama` varchar(48) NOT NULL,
  `harga` int(8) NOT NULL,
  `foto` varchar(128) NOT NULL,
  `stok` int(4) NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` date NOT NULL DEFAULT current_timestamp(),
  `modified_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`id`, `nama`, `harga`, `foto`, `stok`, `keterangan`, `created_at`, `modified_at`) VALUES
(35, 'Will Smith', 30000, '1684501461-2023-05-19-030421.jpg', 22, 'WS', '2023-05-19', '2023-05-19'),
(36, 'Handsome and brave', 250000, '1684501531-2023-05-19-030531.jpg', 15, 'Tampan dan berani', '2023-05-19', '2023-05-19'),
(37, 'Cotton Bud', 3000, '1684847159-2023-05-23-030559.jpg', 40, 'cotton bud', '2023-05-23', '2023-05-23'),
(38, 'Test Produk LCD', 30000, '1686847759-2023-06-15-064919.jpg', 10, 'OK', '2023-06-15', '2023-06-15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk_model`
--

CREATE TABLE `produk_model` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `profil`
--

CREATE TABLE `profil` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `role`
--

CREATE TABLE `role` (
  `id_role` int(11) NOT NULL,
  `nama_role` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `role`
--

INSERT INTO `role` (`id_role`, `nama_role`) VALUES
(1, 'Admin'),
(2, 'Produksi'),
(3, 'Gudang'),
(4, 'Customer');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(10) NOT NULL,
  `nama_user` varchar(128) NOT NULL,
  `alamat` varchar(128) NOT NULL,
  `jk` enum('L','P') NOT NULL,
  `telepon` char(12) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(128) NOT NULL,
  `gambar` varchar(128) NOT NULL,
  `id_role` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `modified_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `nama_user`, `alamat`, `jk`, `telepon`, `email`, `password`, `gambar`, `id_role`, `created_at`, `modified_at`) VALUES
(1, 'PRODUKSI', 'Dsn. Kebondalem - Kandangan', 'L', '085784114468', 'produksi@gmail.com', '$2y$10$FcERMR1U6KDWdPPGcTllgOnbsFTXMD4aFsljDeo.K7wvU.jSsBqEa', 'Screenshot_(29).png', 2, '2022-08-07', '2022-08-07'),
(2, 'SUPERADMIN', 'BANDUNG', 'L', '083822623170', 'superadmin@admin.com', '$2y$10$t2LIGNkyTgoo.wfFq65HU.RMH3.maKSCVMYL1.ix0l.xZjAOfi1PK', 'man-1.png', 1, '2022-06-01', '2022-08-01'),
(3, 'ADMINISTRATOR1', 'Malang', 'L', '085864273099', 'admin@admin.com', '$2y$10$nb12CURwOMBqSon/j1kl0O.oXRIaTIwSWAaS3wzya15kStFeOCo1y', 'kaitokid.jpg', 1, '2022-06-01', '2022-08-07'),
(4, 'User 001', 'Batulawang', 'L', '085864273756', 'user@user.com', '$2y$10$betx1MwS5SC/UacmH8eYgO/zPr2F1848nrAqgPB8Zw65jW4nDdmTS', 'PAP-removebg-preview.png', 4, '2022-06-01', '2022-08-01'),
(6, 'GUDANG', 'Jombang', 'L', '0354328351', 'gudang@gmail.com', '$2y$10$/IE/U/cyw0w9POHggHyNiOQ0U65zeU0k3ILGQ7pSIuJNOtlz0YHt6', 'Ajq2J3.jpg', 3, '2022-08-07', '2022-08-07'),
(7, 'ilmi', 'KANDANGAN - KEDIRI', 'L', '08976543', 'ilmi@gmail.com', '$2y$10$LdQ6C4CedN0CbCnvEbDNvuvS0vqT4aTDpyPyi/5xZLawkRK7jwhW6', 'man-1.png', 4, '2023-05-08', '2023-05-19'),
(8, 'MUHAMMAD ALIYYA ILMI', 'DSN. KEBONDALEM - KANDANGAN - KDR', 'L', '89789798', 'muhammadaliyya19@gmail.com', '$2y$10$nRl3dZPX0CMTxJ7MOQgyqupjfyiZGtxIiOw/KFF.OnHjglYfHPwGu', 'man-1.png', 4, '2023-05-23', '2023-05-23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users_model`
--

CREATE TABLE `users_model` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `auth`
--
ALTER TABLE `auth`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `barang_masuk_model`
--
ALTER TABLE `barang_masuk_model`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `cek_stok_gudang`
--
ALTER TABLE `cek_stok_gudang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `cek_stok_gudang_model`
--
ALTER TABLE `cek_stok_gudang_model`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `dashboard`
--
ALTER TABLE `dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `detil_produk`
--
ALTER TABLE `detil_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pesanan`
--
ALTER TABLE `pesanan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pesanan_model`
--
ALTER TABLE `pesanan_model`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `produk_model`
--
ALTER TABLE `produk_model`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `profil`
--
ALTER TABLE `profil`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indeks untuk tabel `users_model`
--
ALTER TABLE `users_model`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `barang_masuk`
--
ALTER TABLE `barang_masuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `cek_stok_gudang`
--
ALTER TABLE `cek_stok_gudang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `detil_produk`
--
ALTER TABLE `detil_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT untuk tabel `pesanan`
--
ALTER TABLE `pesanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT untuk tabel `role`
--
ALTER TABLE `role`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
